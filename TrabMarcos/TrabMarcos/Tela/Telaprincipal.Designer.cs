﻿namespace TrabMarcos
{
    partial class Telaprincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Telaprincipal));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmcliente = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmfornecedores = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmfuncionario = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmveiculo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmproduto = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmservico = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmmarcasemodelos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmusuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.logoffDeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmoperacoesdevenda = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmoperacoesdevendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmorcamento = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmvendas = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmcompras = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmestoque = new System.Windows.Forms.ToolStripMenuItem();
            this.notasFiscaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmrelatorios = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmcadastro = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmclientes1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmfornecedores1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmfuncionarios1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmcustoseprecos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmmovimentacao = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmcalculadora = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Brown;
            this.toolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.tsmoperacoesdevenda,
            this.tsmrelatorios,
            this.toolStripDropDownButton4,
            this.toolStripDropDownButton5,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmcliente,
            this.tsmfornecedores,
            this.tsmfuncionario,
            this.tsmveiculo,
            this.tsmproduto,
            this.tsmservico,
            this.tsmmarcasemodelos,
            this.tsmusuarios,
            this.logoffDeToolStripMenuItem1,
            this.sairToolStripMenuItem1});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(67, 22);
            this.toolStripDropDownButton1.Text = "Cadastro";
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // tsmcliente
            // 
            this.tsmcliente.Name = "tsmcliente";
            this.tsmcliente.Size = new System.Drawing.Size(201, 22);
            this.tsmcliente.Text = "Clientes                 Ctr+C";
            this.tsmcliente.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // tsmfornecedores
            // 
            this.tsmfornecedores.Name = "tsmfornecedores";
            this.tsmfornecedores.Size = new System.Drawing.Size(201, 22);
            this.tsmfornecedores.Text = "Fornecedores";
            // 
            // tsmfuncionario
            // 
            this.tsmfuncionario.Name = "tsmfuncionario";
            this.tsmfuncionario.Size = new System.Drawing.Size(201, 22);
            this.tsmfuncionario.Text = "Funcionários";
            this.tsmfuncionario.Click += new System.EventHandler(this.tsmfuncionario_Click);
            // 
            // tsmveiculo
            // 
            this.tsmveiculo.Name = "tsmveiculo";
            this.tsmveiculo.Size = new System.Drawing.Size(201, 22);
            this.tsmveiculo.Text = "Veículos                Ctrl+V";
            this.tsmveiculo.Click += new System.EventHandler(this.tsmveiculo_Click);
            // 
            // tsmproduto
            // 
            this.tsmproduto.Name = "tsmproduto";
            this.tsmproduto.Size = new System.Drawing.Size(201, 22);
            this.tsmproduto.Text = "Produtos               Ctrl+P";
            this.tsmproduto.Click += new System.EventHandler(this.tsmproduto_Click);
            // 
            // tsmservico
            // 
            this.tsmservico.Name = "tsmservico";
            this.tsmservico.Size = new System.Drawing.Size(201, 22);
            this.tsmservico.Text = "Serviços";
            // 
            // tsmmarcasemodelos
            // 
            this.tsmmarcasemodelos.Name = "tsmmarcasemodelos";
            this.tsmmarcasemodelos.Size = new System.Drawing.Size(201, 22);
            this.tsmmarcasemodelos.Text = "Marcas e Modelos";
            // 
            // tsmusuarios
            // 
            this.tsmusuarios.Name = "tsmusuarios";
            this.tsmusuarios.Size = new System.Drawing.Size(201, 22);
            this.tsmusuarios.Text = "Usuários";
            this.tsmusuarios.Click += new System.EventHandler(this.tsmusuarios_Click);
            // 
            // logoffDeToolStripMenuItem1
            // 
            this.logoffDeToolStripMenuItem1.Name = "logoffDeToolStripMenuItem1";
            this.logoffDeToolStripMenuItem1.Size = new System.Drawing.Size(201, 22);
            this.logoffDeToolStripMenuItem1.Text = "Logoff de:";
            // 
            // sairToolStripMenuItem1
            // 
            this.sairToolStripMenuItem1.Name = "sairToolStripMenuItem1";
            this.sairToolStripMenuItem1.Size = new System.Drawing.Size(201, 22);
            this.sairToolStripMenuItem1.Text = "Sair";
            // 
            // tsmoperacoesdevenda
            // 
            this.tsmoperacoesdevenda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsmoperacoesdevenda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmoperacoesdevendas,
            this.tsmcompras,
            this.tsmestoque,
            this.notasFiscaisToolStripMenuItem});
            this.tsmoperacoesdevenda.Image = ((System.Drawing.Image)(resources.GetObject("tsmoperacoesdevenda.Image")));
            this.tsmoperacoesdevenda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmoperacoesdevenda.Name = "tsmoperacoesdevenda";
            this.tsmoperacoesdevenda.Size = new System.Drawing.Size(100, 22);
            this.tsmoperacoesdevenda.Text = "Movimentação";
            // 
            // tsmoperacoesdevendas
            // 
            this.tsmoperacoesdevendas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmorcamento,
            this.tsmvendas});
            this.tsmoperacoesdevendas.Name = "tsmoperacoesdevendas";
            this.tsmoperacoesdevendas.Size = new System.Drawing.Size(187, 22);
            this.tsmoperacoesdevendas.Text = "Operações de Vendas";
            // 
            // tsmorcamento
            // 
            this.tsmorcamento.Name = "tsmorcamento";
            this.tsmorcamento.Size = new System.Drawing.Size(134, 22);
            this.tsmorcamento.Text = "Orçamento";
            this.tsmorcamento.Click += new System.EventHandler(this.tsmorcamento_Click);
            // 
            // tsmvendas
            // 
            this.tsmvendas.Name = "tsmvendas";
            this.tsmvendas.Size = new System.Drawing.Size(134, 22);
            this.tsmvendas.Text = "Vendas";
            // 
            // tsmcompras
            // 
            this.tsmcompras.Name = "tsmcompras";
            this.tsmcompras.Size = new System.Drawing.Size(187, 22);
            this.tsmcompras.Text = "Compras";
            // 
            // tsmestoque
            // 
            this.tsmestoque.Name = "tsmestoque";
            this.tsmestoque.Size = new System.Drawing.Size(187, 22);
            this.tsmestoque.Text = "Estoque";
            // 
            // notasFiscaisToolStripMenuItem
            // 
            this.notasFiscaisToolStripMenuItem.Name = "notasFiscaisToolStripMenuItem";
            this.notasFiscaisToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.notasFiscaisToolStripMenuItem.Text = "Notas Fiscais";
            // 
            // tsmrelatorios
            // 
            this.tsmrelatorios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsmrelatorios.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmcadastro,
            this.tsmcustoseprecos,
            this.tsmmovimentacao});
            this.tsmrelatorios.Image = ((System.Drawing.Image)(resources.GetObject("tsmrelatorios.Image")));
            this.tsmrelatorios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmrelatorios.Name = "tsmrelatorios";
            this.tsmrelatorios.Size = new System.Drawing.Size(72, 22);
            this.tsmrelatorios.Text = "Relatórios";
            // 
            // tsmcadastro
            // 
            this.tsmcadastro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmclientes1,
            this.tsmfornecedores1,
            this.tsmfuncionarios1});
            this.tsmcadastro.Name = "tsmcadastro";
            this.tsmcadastro.Size = new System.Drawing.Size(180, 22);
            this.tsmcadastro.Text = "Cadastros";
            // 
            // tsmclientes1
            // 
            this.tsmclientes1.Name = "tsmclientes1";
            this.tsmclientes1.Size = new System.Drawing.Size(145, 22);
            this.tsmclientes1.Text = "Clientes";
            // 
            // tsmfornecedores1
            // 
            this.tsmfornecedores1.Name = "tsmfornecedores1";
            this.tsmfornecedores1.Size = new System.Drawing.Size(145, 22);
            this.tsmfornecedores1.Text = "Fornecedores";
            // 
            // tsmfuncionarios1
            // 
            this.tsmfuncionarios1.Name = "tsmfuncionarios1";
            this.tsmfuncionarios1.Size = new System.Drawing.Size(145, 22);
            this.tsmfuncionarios1.Text = "Funcionários";
            // 
            // tsmcustoseprecos
            // 
            this.tsmcustoseprecos.Name = "tsmcustoseprecos";
            this.tsmcustoseprecos.Size = new System.Drawing.Size(180, 22);
            this.tsmcustoseprecos.Text = "Custos & Preços";
            // 
            // tsmmovimentacao
            // 
            this.tsmmovimentacao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.vendasToolStripMenuItem1,
            this.comprasToolStripMenuItem});
            this.tsmmovimentacao.Name = "tsmmovimentacao";
            this.tsmmovimentacao.Size = new System.Drawing.Size(180, 22);
            this.tsmmovimentacao.Text = "Movimentação";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(122, 22);
            this.toolStripMenuItem5.Text = "Estoque";
            // 
            // vendasToolStripMenuItem1
            // 
            this.vendasToolStripMenuItem1.Name = "vendasToolStripMenuItem1";
            this.vendasToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.vendasToolStripMenuItem1.Text = "Vendas";
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmcalculadora});
            this.toolStripDropDownButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton4.Image")));
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(70, 22);
            this.toolStripDropDownButton4.Text = "Utilitários";
            // 
            // tsmcalculadora
            // 
            this.tsmcalculadora.Name = "tsmcalculadora";
            this.tsmcalculadora.Size = new System.Drawing.Size(180, 22);
            this.tsmcalculadora.Text = "Calculadora";
            // 
            // toolStripDropDownButton5
            // 
            this.toolStripDropDownButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton5.Image")));
            this.toolStripDropDownButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton5.Name = "toolStripDropDownButton5";
            this.toolStripDropDownButton5.Size = new System.Drawing.Size(51, 22);
            this.toolStripDropDownButton5.Text = "Ajuda";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // Telaprincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.BackgroundImage = global::TrabMarcos.Properties.Resources.carro_sigma;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Telaprincipal";
            this.Text = "Telaprincipal";
            this.Load += new System.EventHandler(this.Telaprincipal_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsmcliente;
        private System.Windows.Forms.ToolStripMenuItem tsmfornecedores;
        private System.Windows.Forms.ToolStripMenuItem tsmfuncionario;
        private System.Windows.Forms.ToolStripMenuItem tsmveiculo;
        private System.Windows.Forms.ToolStripMenuItem tsmproduto;
        private System.Windows.Forms.ToolStripMenuItem tsmservico;
        private System.Windows.Forms.ToolStripMenuItem tsmmarcasemodelos;
        private System.Windows.Forms.ToolStripMenuItem tsmusuarios;
        private System.Windows.Forms.ToolStripMenuItem logoffDeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem1;
        private System.Windows.Forms.ToolStripDropDownButton tsmoperacoesdevenda;
        private System.Windows.Forms.ToolStripMenuItem tsmoperacoesdevendas;
        private System.Windows.Forms.ToolStripMenuItem tsmorcamento;
        private System.Windows.Forms.ToolStripMenuItem tsmvendas;
        private System.Windows.Forms.ToolStripMenuItem tsmcompras;
        private System.Windows.Forms.ToolStripMenuItem tsmestoque;
        private System.Windows.Forms.ToolStripMenuItem notasFiscaisToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton tsmrelatorios;
        private System.Windows.Forms.ToolStripMenuItem tsmcadastro;
        private System.Windows.Forms.ToolStripMenuItem tsmcustoseprecos;
        private System.Windows.Forms.ToolStripMenuItem tsmmovimentacao;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton5;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmclientes1;
        private System.Windows.Forms.ToolStripMenuItem tsmfornecedores1;
        private System.Windows.Forms.ToolStripMenuItem tsmfuncionarios1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem vendasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmcalculadora;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}