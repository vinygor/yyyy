﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabMarcos.Todos_os_Databases;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Tela
{
    public partial class Cadastro_Usuário : Form
    {
        public Cadastro_Usuário()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.usuarioinserir == false)
            {

                btninserirusuario.Enabled = false;

            }
            else
            {
                btninserirusuario.Enabled = true;
            }
        }

        private void btnfecharusuario_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void btnalterarusuario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.usuarioalterar == false)
            {

                btnalterarusuario.Enabled = false;

            }
            else
            {
                btnalterarusuario.Enabled = true;
            }
        }

        private void btnexcluirusuario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.usuarioexcluir == false)
            {

                btnexcluirusuario.Enabled = false;

            }
            else
            {
                btnexcluirusuario.Enabled = true;
            }
        }
    }
}
