﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabMarcos.Todas_Business;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Tela
{
    public partial class Cadastro_de_Funcionarios : Form
    {
        public Cadastro_de_Funcionarios()
        {
            InitializeComponent();
        }
        FuncionarioDTO dto;

        private void btnfecharfuncionario_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void Cadastro_de_Funcionarios_Load(object sender, EventArgs e)
        {

        }

        private void btnsalvarfuncionario_Click(object sender, EventArgs e)
        {

            FuncionarioDTO dto = new FuncionarioDTO();

            if (rbnmasculino.Checked == true)
            {

                dto.sexo = "Masculino";

            }
            else if (rbnfeminino.Checked == true)
            {
                dto.sexo = "Feminino";
            }


            dto.nome = txtnm.Text;
            dto.nascimento = DT.Value;
            dto.cpf = txtcpf.Text;
            dto.rg = txtrg.Text;
            dto.complemento = C.Text;
            dto.rua = R.Text;




            dto.foto = ImagemPlugin.ConverterParaString(picest.Image);
            dto.bairro = B.Text;
            dto.cidade = C.Text;
            dto.referencia = R.Text;
            dto.numero = NU.Text;
            dto.estado = CBOE.Text;
            dto.email = E.Text;
            dto.telefone = TEL.Text;
            dto.celular = CEL.Text;
            dto.cep = CEP.Text;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(dto);
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                picest.ImageLocation = dialog.FileName;
            }
        }

        private void btnalterarfuncionario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.funalterar == false)
            {

                btnalterarfuncionario.Enabled = false;

            }
            else
            {
                btnalterarfuncionario.Enabled = true;
            }

            FuncionarioDTO dt = new FuncionarioDTO();
            dt.nome = txtnm.Text;
            dt.nascimento = DT.Value;
            dt.cpf = txtcpf.Text;
            dt.rg = txtrg.Text;
            dt.complemento = C.Text;
            dt.rua = R.Text;




            dt.foto = ImagemPlugin.ConverterParaString(picest.Image);
            dt.bairro = B.Text;
            dt.cidade = C.Text;
            dt.referencia = R.Text;
            dt.numero = NU.Text;
            dt.estado = CBOE.Text;
            dt.email = E.Text;
            dt.telefone = TEL.Text;
            dt.celular = CEL.Text;
            dt.cep = CEP.Text;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Alterar(dt);
        }

        private void btninserirfuncionario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.funinserir == false)
            {

                btninserirfuncionario.Enabled = false;

            }
            else
            {
                btninserirfuncionario.Enabled = true;
            }

            FuncionarioDTO dt = new FuncionarioDTO();
            if (rbnmasculino.Checked == true)
            {

                dt.sexo = "Masculino";

            }
            else if (rbnfeminino.Checked == true)
            {
                dt.sexo = "Feminino";
            }

            dt.nome = txtnm.Text;
            dt.nascimento = DT.Value;
            dt.cpf = txtcpf.Text;
            dt.rg = txtrg.Text;
            dt.complemento = C.Text;
            dt.rua = R.Text;




            dt.foto = ImagemPlugin.ConverterParaString(picest.Image);
            dt.bairro = B.Text;
            dt.cidade = C.Text;
            dt.referencia = R.Text;
            dt.numero = NU.Text;
            dt.estado = CBOE.Text;
            dt.email = E.Text;
            dt.telefone = TEL.Text;
            dt.celular = CEL.Text;
            dt.cep = CEP.Text;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(dt);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                FuncionarioDTO dd = dataGridView1.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Quer mesmo deletar a coluna ?", "SIGMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(dd.id);

                    MessageBox.Show("Funcionario Removido Com Sucesso");


                }
            }
        }

        private void btnexcluirfuncionario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.funexcluir == false)
            {

                btnexcluirfuncionario.Enabled = false;

            }
            else
            {
                btnexcluirfuncionario.Enabled = true;
            }

        }
    }
    
}
