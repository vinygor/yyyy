﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;


namespace TrabMarcos.Tela
{
    public partial class Cadastro_veículos : Form
    {
        public Cadastro_veículos()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void btnfecharveiculo_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void btninserirveiculo_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.veicinserir == false)
            {

                btninserirveiculo.Enabled = false;

            }
            else
            {
                btninserirveiculo.Enabled = true;
            }
        }

        private void btnalterarveiculo_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.veicalterar == false)
            {

                btnalterarveiculo.Enabled = false;

            }
            else
            {
                btnalterarveiculo.Enabled = true;
            }
        }

        private void btnexcluirveiculo_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.veicexcluir == false)
            {

                btnexcluirveiculo.Enabled = false;

            }
            else
            {
                btnexcluirveiculo.Enabled = true;
            }
        }
    }
}
