﻿namespace TrabMarcos.Tela
{
    partial class Cadastro_de_Funcionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fasfaeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Funcionaris = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnsalvarfuncionario = new System.Windows.Forms.Button();
            this.btnfecharfuncionario = new System.Windows.Forms.Button();
            this.btnexcluirfuncionario = new System.Windows.Forms.Button();
            this.btncancelarfuncionario = new System.Windows.Forms.Button();
            this.btnalterarfuncionario = new System.Windows.Forms.Button();
            this.btninserirfuncionario = new System.Windows.Forms.Button();
            this.TEL = new System.Windows.Forms.TextBox();
            this.CEL = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.E = new System.Windows.Forms.TextBox();
            this.picest = new System.Windows.Forms.PictureBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.CBOE = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.CO = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.REF = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CEP = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.NU = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.C = new System.Windows.Forms.TextBox();
            this.B = new System.Windows.Forms.TextBox();
            this.R = new System.Windows.Forms.TextBox();
            this.DT = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.rbnfeminino = new System.Windows.Forms.RadioButton();
            this.rbnmasculino = new System.Windows.Forms.RadioButton();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtrg = new System.Windows.Forms.TextBox();
            this.txtnm = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datanasc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.menuStrip1.SuspendLayout();
            this.Funcionaris.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picest)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fasfaeToolStripMenuItem,
            this.imprimirToolStripMenuItem,
            this.imprimirToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 69;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fasfaeToolStripMenuItem
            // 
            this.fasfaeToolStripMenuItem.BackgroundImage = global::TrabMarcos.Properties.Resources.b47bfb19d11e66c3be00ccb0632047ce_lupa_simples_by_vexels;
            this.fasfaeToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fasfaeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fasfaeToolStripMenuItem.Name = "fasfaeToolStripMenuItem";
            this.fasfaeToolStripMenuItem.Size = new System.Drawing.Size(12, 20);
            this.fasfaeToolStripMenuItem.Text = "Localizar";
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.imprimirToolStripMenuItem.Text = "Localizar";
            // 
            // imprimirToolStripMenuItem1
            // 
            this.imprimirToolStripMenuItem1.Name = "imprimirToolStripMenuItem1";
            this.imprimirToolStripMenuItem1.Size = new System.Drawing.Size(65, 20);
            this.imprimirToolStripMenuItem1.Text = "Imprimir";
            // 
            // Funcionaris
            // 
            this.Funcionaris.AccessibleDescription = "Funcionario";
            this.Funcionaris.AccessibleName = "Funcionario";
            this.Funcionaris.Controls.Add(this.tabPage1);
            this.Funcionaris.Controls.Add(this.tabPage2);
            this.Funcionaris.Location = new System.Drawing.Point(0, 27);
            this.Funcionaris.Name = "Funcionaris";
            this.Funcionaris.SelectedIndex = 0;
            this.Funcionaris.Size = new System.Drawing.Size(800, 425);
            this.Funcionaris.TabIndex = 70;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnsalvarfuncionario);
            this.tabPage1.Controls.Add(this.btnfecharfuncionario);
            this.tabPage1.Controls.Add(this.btnexcluirfuncionario);
            this.tabPage1.Controls.Add(this.btncancelarfuncionario);
            this.tabPage1.Controls.Add(this.btnalterarfuncionario);
            this.tabPage1.Controls.Add(this.btninserirfuncionario);
            this.tabPage1.Controls.Add(this.TEL);
            this.tabPage1.Controls.Add(this.CEL);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.E);
            this.tabPage1.Controls.Add(this.picest);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.comboBox2);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.CBOE);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.CO);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.REF);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.CEP);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.NU);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.C);
            this.tabPage1.Controls.Add(this.B);
            this.tabPage1.Controls.Add(this.R);
            this.tabPage1.Controls.Add(this.DT);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.rbnfeminino);
            this.tabPage1.Controls.Add(this.rbnmasculino);
            this.tabPage1.Controls.Add(this.txtcpf);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.txtrg);
            this.tabPage1.Controls.Add(this.txtnm);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnsalvarfuncionario
            // 
            this.btnsalvarfuncionario.Location = new System.Drawing.Point(630, 362);
            this.btnsalvarfuncionario.Name = "btnsalvarfuncionario";
            this.btnsalvarfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btnsalvarfuncionario.TabIndex = 157;
            this.btnsalvarfuncionario.Text = "Salvar";
            this.btnsalvarfuncionario.UseVisualStyleBackColor = true;
            this.btnsalvarfuncionario.Click += new System.EventHandler(this.btnsalvarfuncionario_Click);
            // 
            // btnfecharfuncionario
            // 
            this.btnfecharfuncionario.Location = new System.Drawing.Point(708, 362);
            this.btnfecharfuncionario.Name = "btnfecharfuncionario";
            this.btnfecharfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btnfecharfuncionario.TabIndex = 156;
            this.btnfecharfuncionario.Text = "Fechar";
            this.btnfecharfuncionario.UseVisualStyleBackColor = true;
            // 
            // btnexcluirfuncionario
            // 
            this.btnexcluirfuncionario.Location = new System.Drawing.Point(237, 362);
            this.btnexcluirfuncionario.Name = "btnexcluirfuncionario";
            this.btnexcluirfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btnexcluirfuncionario.TabIndex = 155;
            this.btnexcluirfuncionario.Text = "Excuir";
            this.btnexcluirfuncionario.UseVisualStyleBackColor = true;
            this.btnexcluirfuncionario.Click += new System.EventHandler(this.btnexcluirfuncionario_Click);
            // 
            // btncancelarfuncionario
            // 
            this.btncancelarfuncionario.Location = new System.Drawing.Point(159, 362);
            this.btncancelarfuncionario.Name = "btncancelarfuncionario";
            this.btncancelarfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btncancelarfuncionario.TabIndex = 154;
            this.btncancelarfuncionario.Text = "Cancelar";
            this.btncancelarfuncionario.UseVisualStyleBackColor = true;
            // 
            // btnalterarfuncionario
            // 
            this.btnalterarfuncionario.Location = new System.Drawing.Point(81, 362);
            this.btnalterarfuncionario.Name = "btnalterarfuncionario";
            this.btnalterarfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btnalterarfuncionario.TabIndex = 153;
            this.btnalterarfuncionario.Text = "Alterar";
            this.btnalterarfuncionario.UseVisualStyleBackColor = true;
            this.btnalterarfuncionario.Click += new System.EventHandler(this.btnalterarfuncionario_Click);
            // 
            // btninserirfuncionario
            // 
            this.btninserirfuncionario.Location = new System.Drawing.Point(4, 362);
            this.btninserirfuncionario.Name = "btninserirfuncionario";
            this.btninserirfuncionario.Size = new System.Drawing.Size(72, 36);
            this.btninserirfuncionario.TabIndex = 152;
            this.btninserirfuncionario.Text = "Inserir";
            this.btninserirfuncionario.UseVisualStyleBackColor = true;
            this.btninserirfuncionario.Click += new System.EventHandler(this.btninserirfuncionario_Click);
            // 
            // TEL
            // 
            this.TEL.Location = new System.Drawing.Point(630, 302);
            this.TEL.Name = "TEL";
            this.TEL.Size = new System.Drawing.Size(131, 20);
            this.TEL.TabIndex = 151;
            // 
            // CEL
            // 
            this.CEL.Location = new System.Drawing.Point(630, 328);
            this.CEL.Name = "CEL";
            this.CEL.Size = new System.Drawing.Size(131, 20);
            this.CEL.TabIndex = 150;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(580, 331);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(49, 14);
            this.label24.TabIndex = 149;
            this.label24.Text = "Celular:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(571, 305);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 14);
            this.label23.TabIndex = 148;
            this.label23.Text = "Telefone:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label22.Location = new System.Drawing.Point(364, 317);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(117, 13);
            this.label22.TabIndex = 147;
            this.label22.Text = "Ex: fulano@hitmail.com";
            // 
            // E
            // 
            this.E.Location = new System.Drawing.Point(81, 314);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(277, 20);
            this.E.TabIndex = 146;
            // 
            // picest
            // 
            this.picest.Location = new System.Drawing.Point(643, 1);
            this.picest.Name = "picest";
            this.picest.Size = new System.Drawing.Size(146, 166);
            this.picest.TabIndex = 145;
            this.picest.TabStop = false;
            this.picest.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label21.Location = new System.Drawing.Point(560, 107);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 13);
            this.label21.TabIndex = 144;
            this.label21.Text = "dd/mm/aaaa";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(474, 14);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(116, 21);
            this.comboBox2.TabIndex = 143;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(377, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 14);
            this.label20.TabIndex = 142;
            this.label20.Text = "Departamento:";
            // 
            // CBOE
            // 
            this.CBOE.FormattingEnabled = true;
            this.CBOE.Location = new System.Drawing.Point(744, 224);
            this.CBOE.Name = "CBOE";
            this.CBOE.Size = new System.Drawing.Size(36, 21);
            this.CBOE.TabIndex = 141;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(677, 226);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 15);
            this.label19.TabIndex = 140;
            this.label19.Text = "Estado:";
            // 
            // CO
            // 
            this.CO.Location = new System.Drawing.Point(640, 199);
            this.CO.Name = "CO";
            this.CO.Size = new System.Drawing.Size(140, 20);
            this.CO.TabIndex = 139;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(548, 200);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 15);
            this.label18.TabIndex = 138;
            this.label18.Text = "Complemento:";
            // 
            // REF
            // 
            this.REF.Location = new System.Drawing.Point(380, 251);
            this.REF.Name = "REF";
            this.REF.Size = new System.Drawing.Size(400, 20);
            this.REF.TabIndex = 137;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(298, 252);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 15);
            this.label17.TabIndex = 136;
            this.label17.Text = "Referências:";
            // 
            // CEP
            // 
            this.CEP.Location = new System.Drawing.Point(440, 225);
            this.CEP.Name = "CEP";
            this.CEP.Size = new System.Drawing.Size(102, 20);
            this.CEP.TabIndex = 135;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(400, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(33, 15);
            this.label16.TabIndex = 134;
            this.label16.Text = "CEP:";
            // 
            // NU
            // 
            this.NU.Location = new System.Drawing.Point(461, 199);
            this.NU.Name = "NU";
            this.NU.Size = new System.Drawing.Size(72, 20);
            this.NU.TabIndex = 133;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(400, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 15);
            this.label15.TabIndex = 132;
            this.label15.Text = "Número:";
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(81, 252);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(189, 20);
            this.C.TabIndex = 131;
            this.C.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(81, 226);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(212, 20);
            this.B.TabIndex = 130;
            // 
            // R
            // 
            this.R.Location = new System.Drawing.Point(81, 200);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(300, 20);
            this.R.TabIndex = 129;
            // 
            // DT
            // 
            this.DT.Location = new System.Drawing.Point(389, 104);
            this.DT.Name = "DT";
            this.DT.Size = new System.Drawing.Size(166, 20);
            this.DT.TabIndex = 128;
            this.DT.Value = new System.DateTime(2018, 8, 25, 12, 22, 13, 0);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(259, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 14);
            this.label14.TabIndex = 127;
            this.label14.Text = "Data de Nascimento:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(81, 134);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(472, 20);
            this.textBox4.TabIndex = 126;
            // 
            // rbnfeminino
            // 
            this.rbnfeminino.AutoSize = true;
            this.rbnfeminino.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnfeminino.Location = new System.Drawing.Point(168, 104);
            this.rbnfeminino.Name = "rbnfeminino";
            this.rbnfeminino.Size = new System.Drawing.Size(76, 18);
            this.rbnfeminino.TabIndex = 125;
            this.rbnfeminino.TabStop = true;
            this.rbnfeminino.Text = "Feminino";
            this.rbnfeminino.UseVisualStyleBackColor = true;
            // 
            // rbnmasculino
            // 
            this.rbnmasculino.AutoSize = true;
            this.rbnmasculino.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnmasculino.Location = new System.Drawing.Point(89, 104);
            this.rbnmasculino.Name = "rbnmasculino";
            this.rbnmasculino.Size = new System.Drawing.Size(81, 18);
            this.rbnmasculino.TabIndex = 124;
            this.rbnmasculino.TabStop = true;
            this.rbnmasculino.Text = "Masculino";
            this.rbnmasculino.UseVisualStyleBackColor = true;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(262, 68);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(121, 20);
            this.txtcpf.TabIndex = 123;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(223, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 15);
            this.label13.TabIndex = 122;
            this.label13.Text = "CPF:";
            // 
            // txtrg
            // 
            this.txtrg.Location = new System.Drawing.Point(81, 68);
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(113, 20);
            this.txtrg.TabIndex = 121;
            // 
            // txtnm
            // 
            this.txtnm.Location = new System.Drawing.Point(81, 42);
            this.txtnm.Name = "txtnm";
            this.txtnm.Size = new System.Drawing.Size(302, 20);
            this.txtnm.TabIndex = 120;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(34, 316);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 14);
            this.label12.TabIndex = 119;
            this.label12.Text = "Email:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 289);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 14);
            this.label11.TabIndex = 118;
            this.label11.Text = "Contato";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(27, 253);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 14);
            this.label10.TabIndex = 117;
            this.label10.Text = "Cidade:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(33, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 14);
            this.label9.TabIndex = 116;
            this.label9.Text = "Bairro:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(43, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 14);
            this.label8.TabIndex = 115;
            this.label8.Text = "Rua:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 14);
            this.label7.TabIndex = 114;
            this.label7.Text = "Endereço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(41, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 14);
            this.label6.TabIndex = 113;
            this.label6.Text = "Foto:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(33, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 14);
            this.label5.TabIndex = 112;
            this.label5.Text = "Sexo :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(46, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 14);
            this.label4.TabIndex = 111;
            this.label4.Text = "RG :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(32, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 110;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.TabIndex = 109;
            this.label2.Text = "Código :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 14);
            this.label1.TabIndex = 108;
            this.label1.Text = "Funcionario";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Nome,
            this.cpf,
            this.rg,
            this.datanasc,
            this.Column1});
            this.dataGridView1.Location = new System.Drawing.Point(0, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(795, 371);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // cpf
            // 
            this.cpf.DataPropertyName = "cpf";
            this.cpf.HeaderText = "Cpf";
            this.cpf.Name = "cpf";
            // 
            // rg
            // 
            this.rg.DataPropertyName = "rg";
            this.rg.HeaderText = "rg";
            this.rg.Name = "rg";
            // 
            // datanasc
            // 
            this.datanasc.DataPropertyName = "nascimento";
            this.datanasc.HeaderText = "datanasc";
            this.datanasc.Name = "datanasc";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Image = global::TrabMarcos.Properties.Resources.download;
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::TrabMarcos.Properties.Resources.download;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Cadastro_de_Funcionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Funcionaris);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Cadastro_de_Funcionarios";
            this.Text = "Cadastro_de_Funcionarios";
            this.Load += new System.EventHandler(this.Cadastro_de_Funcionarios_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.Funcionaris.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picest)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fasfaeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem1;
        private System.Windows.Forms.TabControl Funcionaris;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnsalvarfuncionario;
        private System.Windows.Forms.Button btnfecharfuncionario;
        private System.Windows.Forms.Button btnexcluirfuncionario;
        private System.Windows.Forms.Button btncancelarfuncionario;
        private System.Windows.Forms.Button btnalterarfuncionario;
        private System.Windows.Forms.Button btninserirfuncionario;
        private System.Windows.Forms.TextBox TEL;
        private System.Windows.Forms.TextBox CEL;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox E;
        private System.Windows.Forms.PictureBox picest;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox CBOE;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox CO;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox REF;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox CEP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox NU;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox C;
        private System.Windows.Forms.TextBox B;
        private System.Windows.Forms.TextBox R;
        private System.Windows.Forms.DateTimePicker DT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RadioButton rbnfeminino;
        private System.Windows.Forms.RadioButton rbnmasculino;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtrg;
        private System.Windows.Forms.TextBox txtnm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn rg;
        private System.Windows.Forms.DataGridViewTextBoxColumn datanasc;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
    }
}