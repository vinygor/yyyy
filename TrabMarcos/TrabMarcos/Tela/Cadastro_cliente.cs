﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabMarcos.pasta;
using TrabMarcos.Todas_Business;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Tela
{
    public partial class Cadastro_cliente : Form
    {
        validacoes v = new validacoes();
        public Cadastro_cliente()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                clienteDTO dto = new clienteDTO();

                if (txtnome.Text != string.Empty)
                {
                    dto.Nome = txtnome.Text;
                }
                if (txtCPF.Text != string.Empty)
                {
                    dto.CPF = txtCPF.Text;
                }
                if (txtbairro.Text != string.Empty)
                {
                    dto.bairro = txtbairro.Text;
                }
                if (txtprofi.Text != string.Empty)
                {
                    dto.profissao = txtprofi.Text;
                }
                if (txtrua.Text != string.Empty)
                {
                    dto.rua = txtrua.Text;
                }
                if (txtcidade.Text != string.Empty)
                {
                    dto.cidade = txtcidade.Text;
                }
                if (txtnumero.Text != string.Empty)
                {
                    dto.numero = txtnumero.Text;
                }
                if (txtCPE.Text != string.Empty)
                {
                    dto.CEP = txtCPE.Text;
                }
                if (txtreferencia.Text != string.Empty)
                {
                    dto.referencia = txtreferencia.Text;
                }
                if (txtcomplemento.Text != string.Empty)
                {
                    dto.complemento = txtcomplemento.Text;
                }
                if (txtestado.Text != string.Empty)
                {
                    dto.estado = txtestado.Text;
                }
                if (txttelefone1.Text != string.Empty)
                {
                    dto.telefone = txttelefone1.Text;
                }
                if (mtbcelular1.Text != string.Empty)
                {
                    dto.celular1 = mtbcelular1.Text;
                }
                if (mtbtc.Text != string.Empty)
                {
                    dto.telefonecormecial = mtbtc.Text;
                }
                if (mtbemail.Text != string.Empty)
                {
                    dto.email = mtbemail.Text;
                }
                if (txtramal.Text != string.Empty)
                {
                    dto.ramal = txtramal.Text;
                }
                if (mtbtelefone2.Text != string.Empty)
                {
                    dto.telefone2 = txttelefone1.Text;
                }
                if (mtbcelular2.Text != string.Empty)
                {
                    dto.celular2 = mtbcelular2.Text;
                }
                else
                {
                    MessageBox.Show("Erro! Todos os campos devem estar devidamente preenchidos");
                    
                }
                clienteBusiness business = new clienteBusiness();
                business.Salvar(dto);
                return;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                   "Chinelo",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }

            

            MessageBox.Show("Cliente salvo com sucesso.", "Lojachinelosanderson", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void dtpnascimento_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnome_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtcpf_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtprofissao_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void mkbdatanasc_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtrua_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtcidade_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtnumero_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtcep_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtreferencia_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtestado_keypress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txttelefone_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void mkbcelular_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void mkb_telefonecomercial_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtramal_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void mkbtelefone2_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void mkbcelular2_keypress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.cliinserir == false)
            {

                btninserircliente.Enabled = false;

            }
            else
            {
                btninserircliente.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.clialterar == false)
            {

                btnalterarcliente.Enabled = false;

            }
            else
            {
                btnalterarcliente.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            

          
        }
    }
}
