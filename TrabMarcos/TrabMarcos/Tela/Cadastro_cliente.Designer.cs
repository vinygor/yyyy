﻿namespace TrabMarcos.Tela
{
    partial class Cadastro_cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cadastro_cliente));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.fe = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.mtbtc = new System.Windows.Forms.MaskedTextBox();
            this.mtbcelular1 = new System.Windows.Forms.MaskedTextBox();
            this.txtramal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.mtbemail = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txttelefone1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtreferencia = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtestado = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.d = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCPE = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtco = new System.Windows.Forms.TextBox();
            this.dtpnascimento = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mtbdatanas = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtprofi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnsalvarcliente = new System.Windows.Forms.Button();
            this.btnfecharcliente = new System.Windows.Forms.Button();
            this.btnexcluircliente = new System.Windows.Forms.Button();
            this.btncancelarcliente = new System.Windows.Forms.Button();
            this.btnalterarcliente = new System.Windows.Forms.Button();
            this.btninserircliente = new System.Windows.Forms.Button();
            this.mtbcelular2 = new System.Windows.Forms.MaskedTextBox();
            this.mtbtelefone2 = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1.SuspendLayout();
            this.fe.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripLabel1,
            this.toolStripButton2,
            this.toolStripLabel2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(899, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(53, 22);
            this.toolStripLabel1.Text = "Localizar";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(53, 22);
            this.toolStripLabel2.Text = "imprimir";
            // 
            // fe
            // 
            this.fe.Controls.Add(this.tabPage1);
            this.fe.Controls.Add(this.tabPage2);
            this.fe.Location = new System.Drawing.Point(4, 22);
            this.fe.Name = "fe";
            this.fe.SelectedIndex = 0;
            this.fe.Size = new System.Drawing.Size(890, 682);
            this.fe.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(882, 656);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados gerais";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(745, 484);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(129, 37);
            this.button6.TabIndex = 41;
            this.button6.Text = "Fechar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(616, 484);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(129, 37);
            this.button5.TabIndex = 40;
            this.button5.Text = "Salvar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(277, 484);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 37);
            this.button3.TabIndex = 38;
            this.button3.Text = "Cancelar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(142, 484);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(129, 37);
            this.button2.TabIndex = 37;
            this.button2.Text = "Alterar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gainsboro;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(8, 484);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 37);
            this.button1.TabIndex = 36;
            this.button1.Text = "Inserir";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label26.Location = new System.Drawing.Point(18, 355);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 18);
            this.label26.TabIndex = 35;
            this.label26.Text = "Contato";
            this.label26.UseWaitCursor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.mtbtc);
            this.panel2.Controls.Add(this.mtbcelular1);
            this.panel2.Controls.Add(this.txtramal);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.mtbemail);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.txttelefone1);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Location = new System.Drawing.Point(8, 376);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(866, 91);
            this.panel2.TabIndex = 34;
            this.panel2.UseWaitCursor = true;
            // 
            // mtbtc
            // 
            this.mtbtc.Location = new System.Drawing.Point(439, 19);
            this.mtbtc.Mask = "(99)9999-9999";
            this.mtbtc.Name = "mtbtc";
            this.mtbtc.Size = new System.Drawing.Size(195, 22);
            this.mtbtc.TabIndex = 31;
            this.mtbtc.UseWaitCursor = true;
            this.mtbtc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mkb_telefonecomercial_keypress);
            // 
            // mtbcelular1
            // 
            this.mtbcelular1.Location = new System.Drawing.Point(67, 51);
            this.mtbcelular1.Mask = "(99)99-99999";
            this.mtbcelular1.Name = "mtbcelular1";
            this.mtbcelular1.Size = new System.Drawing.Size(195, 22);
            this.mtbcelular1.TabIndex = 24;
            this.mtbcelular1.UseWaitCursor = true;
            this.mtbcelular1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mkbcelular_keypress);
            // 
            // txtramal
            // 
            this.txtramal.Location = new System.Drawing.Point(732, 15);
            this.txtramal.Name = "txtramal";
            this.txtramal.Size = new System.Drawing.Size(103, 22);
            this.txtramal.TabIndex = 30;
            this.txtramal.UseWaitCursor = true;
            this.txtramal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtramal_keypress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(669, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "Ramal";
            this.label14.UseWaitCursor = true;
            // 
            // mtbemail
            // 
            this.mtbemail.Location = new System.Drawing.Point(355, 51);
            this.mtbemail.Name = "mtbemail";
            this.mtbemail.Size = new System.Drawing.Size(360, 22);
            this.mtbemail.TabIndex = 28;
            this.mtbemail.UseWaitCursor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(307, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(126, 18);
            this.label17.TabIndex = 27;
            this.label17.Text = "Telefone comercial";
            this.label17.UseWaitCursor = true;
            // 
            // txttelefone1
            // 
            this.txttelefone1.Location = new System.Drawing.Point(71, 15);
            this.txttelefone1.Name = "txttelefone1";
            this.txttelefone1.Size = new System.Drawing.Size(195, 22);
            this.txttelefone1.TabIndex = 25;
            this.txttelefone1.UseWaitCursor = true;
            this.txttelefone1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttelefone_keypress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(307, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 18);
            this.label18.TabIndex = 15;
            this.label18.Text = "Email";
            this.label18.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(9, 19);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 18);
            this.label20.TabIndex = 2;
            this.label20.Text = "Telefone";
            this.label20.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(9, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 18);
            this.label22.TabIndex = 3;
            this.label22.Text = "Celular";
            this.label22.UseWaitCursor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(18, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 18);
            this.label11.TabIndex = 25;
            this.label11.Text = "Endereço";
            this.label11.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.txtreferencia);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtestado);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.txtcomplemento);
            this.panel3.Controls.Add(this.d);
            this.panel3.Controls.Add(this.txtnumero);
            this.panel3.Controls.Add(this.txtrua);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.txtCPE);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.txtbairro);
            this.panel3.Controls.Add(this.txtcidade);
            this.panel3.Location = new System.Drawing.Point(8, 230);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(866, 122);
            this.panel3.TabIndex = 24;
            this.panel3.UseWaitCursor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(445, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 18);
            this.label13.TabIndex = 33;
            this.label13.Text = "Referência";
            this.label13.UseWaitCursor = true;
            // 
            // txtreferencia
            // 
            this.txtreferencia.Location = new System.Drawing.Point(526, 79);
            this.txtreferencia.Name = "txtreferencia";
            this.txtreferencia.Size = new System.Drawing.Size(210, 22);
            this.txtreferencia.TabIndex = 32;
            this.txtreferencia.UseWaitCursor = true;
            this.txtreferencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtreferencia_keypress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(644, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 18);
            this.label9.TabIndex = 30;
            this.label9.Text = "Estado";
            this.label9.UseWaitCursor = true;
            // 
            // txtestado
            // 
            this.txtestado.Location = new System.Drawing.Point(699, 47);
            this.txtestado.Name = "txtestado";
            this.txtestado.Size = new System.Drawing.Size(138, 22);
            this.txtestado.TabIndex = 31;
            this.txtestado.UseWaitCursor = true;
            this.txtestado.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            this.txtestado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtestado_keypress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(618, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 29;
            this.label12.Text = "Complemento";
            this.label12.UseWaitCursor = true;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(721, 15);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(114, 22);
            this.txtcomplemento.TabIndex = 28;
            this.txtcomplemento.UseWaitCursor = true;
            // 
            // d
            // 
            this.d.AutoSize = true;
            this.d.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d.ForeColor = System.Drawing.SystemColors.ControlText;
            this.d.Location = new System.Drawing.Point(461, 19);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(59, 18);
            this.d.TabIndex = 27;
            this.d.Text = "Numero";
            this.d.UseWaitCursor = true;
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(526, 18);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(86, 22);
            this.txtnumero.TabIndex = 26;
            this.txtnumero.UseWaitCursor = true;
            this.txtnumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumero_keypress);
            // 
            // txtrua
            // 
            this.txtrua.Location = new System.Drawing.Point(67, 18);
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(388, 22);
            this.txtrua.TabIndex = 25;
            this.txtrua.UseWaitCursor = true;
            this.txtrua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrua_keypress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(489, 51);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 18);
            this.label21.TabIndex = 15;
            this.label21.Text = "CEP";
            this.label21.UseWaitCursor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(14, 83);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(51, 18);
            this.label23.TabIndex = 11;
            this.label23.Text = "Cidade";
            this.label23.UseWaitCursor = true;
            // 
            // txtCPE
            // 
            this.txtCPE.Location = new System.Drawing.Point(526, 51);
            this.txtCPE.Name = "txtCPE";
            this.txtCPE.Size = new System.Drawing.Size(110, 22);
            this.txtCPE.TabIndex = 10;
            this.txtCPE.UseWaitCursor = true;
            this.txtCPE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcep_keypress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(14, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 18);
            this.label24.TabIndex = 2;
            this.label24.Text = "Rua";
            this.label24.UseWaitCursor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label25.Location = new System.Drawing.Point(14, 55);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 18);
            this.label25.TabIndex = 3;
            this.label25.Text = "Bairro";
            this.label25.UseWaitCursor = true;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(67, 51);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(240, 22);
            this.txtbairro.TabIndex = 4;
            this.txtbairro.UseWaitCursor = true;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(67, 79);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(210, 22);
            this.txtcidade.TabIndex = 5;
            this.txtcidade.UseWaitCursor = true;
            this.txtcidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcidade_keypress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(18, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "cliente";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtco);
            this.panel1.Controls.Add(this.dtpnascimento);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.mtbdatanas);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtprofi);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtnome);
            this.panel1.Controls.Add(this.txtCPF);
            this.panel1.Location = new System.Drawing.Point(8, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(866, 167);
            this.panel1.TabIndex = 11;
            this.panel1.UseWaitCursor = true;
            // 
            // txtco
            // 
            this.txtco.Location = new System.Drawing.Point(71, 18);
            this.txtco.Name = "txtco";
            this.txtco.Size = new System.Drawing.Size(47, 22);
            this.txtco.TabIndex = 23;
            this.txtco.UseWaitCursor = true;
            this.txtco.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // dtpnascimento
            // 
            this.dtpnascimento.Location = new System.Drawing.Point(618, 122);
            this.dtpnascimento.Name = "dtpnascimento";
            this.dtpnascimento.Size = new System.Drawing.Size(217, 22);
            this.dtpnascimento.TabIndex = 22;
            this.dtpnascimento.UseWaitCursor = true;
            this.dtpnascimento.ValueChanged += new System.EventHandler(this.dtpnascimento_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(518, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Cliente desde";
            this.label10.UseWaitCursor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(680, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "dd/mm/aaaa";
            this.label8.UseWaitCursor = true;
            // 
            // mtbdatanas
            // 
            this.mtbdatanas.Location = new System.Drawing.Point(543, 83);
            this.mtbdatanas.Mask = "99/99/9999";
            this.mtbdatanas.Name = "mtbdatanas";
            this.mtbdatanas.Size = new System.Drawing.Size(100, 22);
            this.mtbdatanas.TabIndex = 12;
            this.mtbdatanas.UseWaitCursor = true;
            this.mtbdatanas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mkbdatanasc_keypress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(407, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Data de nascimento";
            this.label7.UseWaitCursor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(659, 18);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(111, 19);
            this.radioButton2.TabIndex = 17;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Pessoa Juridica";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.UseWaitCursor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(541, 18);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(100, 19);
            this.radioButton1.TabIndex = 16;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Pessoa Fisica";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.UseWaitCursor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(473, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Profissão";
            this.label6.UseWaitCursor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(158, 129);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(85, 19);
            this.checkBox2.TabIndex = 14;
            this.checkBox2.Text = "Masculino";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.UseWaitCursor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(67, 129);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(85, 19);
            this.checkBox1.TabIndex = 13;
            this.checkBox1.Text = "Masculino";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.UseWaitCursor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(14, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "Sexo";
            this.label5.UseWaitCursor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(14, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "CPF";
            this.label4.UseWaitCursor = true;
            // 
            // txtprofi
            // 
            this.txtprofi.Location = new System.Drawing.Point(541, 51);
            this.txtprofi.Name = "txtprofi";
            this.txtprofi.Size = new System.Drawing.Size(294, 22);
            this.txtprofi.TabIndex = 10;
            this.txtprofi.UseWaitCursor = true;
            this.txtprofi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprofissao_keypress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(14, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Codigo";
            this.label1.UseWaitCursor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(14, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nome";
            this.label3.UseWaitCursor = true;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(67, 51);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(388, 22);
            this.txtnome.TabIndex = 4;
            this.txtnome.UseWaitCursor = true;
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_keypress);
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(67, 79);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(176, 22);
            this.txtCPF.TabIndex = 5;
            this.txtCPF.UseWaitCursor = true;
            this.txtCPF.TextChanged += new System.EventHandler(this.txtCPF_TextChanged);
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcpf_keypress);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage2.Controls.Add(this.btnsalvarcliente);
            this.tabPage2.Controls.Add(this.btnfecharcliente);
            this.tabPage2.Controls.Add(this.btnexcluircliente);
            this.tabPage2.Controls.Add(this.btncancelarcliente);
            this.tabPage2.Controls.Add(this.btnalterarcliente);
            this.tabPage2.Controls.Add(this.btninserircliente);
            this.tabPage2.Controls.Add(this.mtbcelular2);
            this.tabPage2.Controls.Add(this.mtbtelefone2);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.textBox16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(882, 656);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Observações";
            // 
            // btnsalvarcliente
            // 
            this.btnsalvarcliente.Location = new System.Drawing.Point(601, 470);
            this.btnsalvarcliente.Name = "btnsalvarcliente";
            this.btnsalvarcliente.Size = new System.Drawing.Size(129, 37);
            this.btnsalvarcliente.TabIndex = 13;
            this.btnsalvarcliente.Text = "Salvar";
            this.btnsalvarcliente.UseVisualStyleBackColor = true;
            // 
            // btnfecharcliente
            // 
            this.btnfecharcliente.Location = new System.Drawing.Point(736, 470);
            this.btnfecharcliente.Name = "btnfecharcliente";
            this.btnfecharcliente.Size = new System.Drawing.Size(129, 37);
            this.btnfecharcliente.TabIndex = 12;
            this.btnfecharcliente.Text = "fechar";
            this.btnfecharcliente.UseVisualStyleBackColor = true;
            // 
            // btnexcluircliente
            // 
            this.btnexcluircliente.Location = new System.Drawing.Point(431, 470);
            this.btnexcluircliente.Name = "btnexcluircliente";
            this.btnexcluircliente.Size = new System.Drawing.Size(129, 37);
            this.btnexcluircliente.TabIndex = 11;
            this.btnexcluircliente.Text = "Encluir";
            this.btnexcluircliente.UseVisualStyleBackColor = true;
            // 
            // btncancelarcliente
            // 
            this.btncancelarcliente.Location = new System.Drawing.Point(296, 470);
            this.btncancelarcliente.Name = "btncancelarcliente";
            this.btncancelarcliente.Size = new System.Drawing.Size(129, 37);
            this.btncancelarcliente.TabIndex = 10;
            this.btncancelarcliente.Text = "Cancelar";
            this.btncancelarcliente.UseVisualStyleBackColor = true;
            // 
            // btnalterarcliente
            // 
            this.btnalterarcliente.Location = new System.Drawing.Point(161, 470);
            this.btnalterarcliente.Name = "btnalterarcliente";
            this.btnalterarcliente.Size = new System.Drawing.Size(129, 37);
            this.btnalterarcliente.TabIndex = 9;
            this.btnalterarcliente.Text = "Alterar";
            this.btnalterarcliente.UseVisualStyleBackColor = true;
            // 
            // btninserircliente
            // 
            this.btninserircliente.Location = new System.Drawing.Point(25, 470);
            this.btninserircliente.Name = "btninserircliente";
            this.btninserircliente.Size = new System.Drawing.Size(129, 37);
            this.btninserircliente.TabIndex = 8;
            this.btninserircliente.Text = "Inserir";
            this.btninserircliente.UseVisualStyleBackColor = true;
            // 
            // mtbcelular2
            // 
            this.mtbcelular2.Location = new System.Drawing.Point(135, 270);
            this.mtbcelular2.Mask = "(99)99-99999";
            this.mtbcelular2.Name = "mtbcelular2";
            this.mtbcelular2.Size = new System.Drawing.Size(155, 22);
            this.mtbcelular2.TabIndex = 7;
            this.mtbcelular2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mkbcelular2_keypress);
            // 
            // mtbtelefone2
            // 
            this.mtbtelefone2.Location = new System.Drawing.Point(135, 240);
            this.mtbtelefone2.Mask = "(99)9999-9999";
            this.mtbtelefone2.Name = "mtbtelefone2";
            this.mtbtelefone2.Size = new System.Drawing.Size(155, 22);
            this.mtbtelefone2.TabIndex = 6;
            this.mtbtelefone2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mkbtelefone2_keypress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 270);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 18);
            this.label27.TabIndex = 5;
            this.label27.Text = "Celular 2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(22, 243);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 18);
            this.label19.TabIndex = 4;
            this.label19.Text = "Telefone 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 214);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 18);
            this.label16.TabIndex = 3;
            this.label16.Text = "Outros contatos";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(15, 43);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(850, 150);
            this.textBox16.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(245, 18);
            this.label15.TabIndex = 0;
            this.label15.Text = "Anotaçoes do cliente : (300 caracteres)";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Cadastro_cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 583);
            this.Controls.Add(this.fe);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Cadastro_cliente";
            this.Text = "Cadastro_Cliente";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.fe.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.TabControl fe;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MaskedTextBox mtbtc;
        private System.Windows.Forms.MaskedTextBox mtbcelular1;
        private System.Windows.Forms.TextBox txtramal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox mtbemail;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txttelefone1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtreferencia;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtestado;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label d;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtCPE;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtco;
        private System.Windows.Forms.DateTimePicker dtpnascimento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mtbdatanas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtprofi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnsalvarcliente;
        private System.Windows.Forms.Button btnfecharcliente;
        private System.Windows.Forms.Button btnexcluircliente;
        private System.Windows.Forms.Button btncancelarcliente;
        private System.Windows.Forms.Button btnalterarcliente;
        private System.Windows.Forms.Button btninserircliente;
        private System.Windows.Forms.MaskedTextBox mtbcelular2;
        private System.Windows.Forms.MaskedTextBox mtbtelefone2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}