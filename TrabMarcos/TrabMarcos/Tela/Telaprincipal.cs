﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrabMarcos.Tela;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;
using TrabMarcos.Todos_os_Databases;

namespace TrabMarcos
{
    public partial class Telaprincipal : Form
    {
        public Telaprincipal()
        {
            InitializeComponent();
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Telaprincipal_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.cliacess == false)
            {

                tsmcliente.Enabled = false;

            }
            else
            {
                tsmcliente.Enabled = true;
            }

            Cadastro_cliente form = new Cadastro_cliente();
            form.Show();
            
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void relacionamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tsmfuncionario_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.funacess == false)
            {

                tsmfuncionario.Enabled = false;

            }
            else
            {
                tsmfuncionario.Enabled = true;
            }

            Cadastro_de_Funcionarios form = new Cadastro_de_Funcionarios();
            form.Show();
         
        }

        private void tsmveiculo_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.veicacess == false)
            {

                tsmveiculo.Enabled = false;

            }
            else
            {
                tsmveiculo.Enabled = true;
            }

            Cadastro_veículos form = new Cadastro_veículos();
            form.Show();
            
        }

        private void tsmorcamento_Click(object sender, EventArgs e)
        {
            

            Orçamento form = new Orçamento();
            form.Show();
           
        }

        private void tsmusuarios_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.usuarioacess == false)
            {

                tsmusuarios.Enabled = false;

            }
            else
            {
                tsmusuarios.Enabled = true;
            }
        }

        private void tsmproduto_Click(object sender, EventArgs e)
        {
            perfilDTO dto = new perfilDTO();

            if (dto.proacess == false)
            {

                tsmproduto.Enabled = false;

            }
            else
            {
                tsmproduto.Enabled = true;
            }
        }
    }
}
