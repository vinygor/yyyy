﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class GrupomodelosDTO
    {
        public CodigogrupomodelosDTO id { get; set; }
        public FabricanteDTO fabricante { get; set; }

    }
}
