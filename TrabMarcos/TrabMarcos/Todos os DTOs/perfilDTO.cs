﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class perfilDTO
    {
        public int id { get; set; }
        public string perfil { get; set; }
        public string nivel { get; set; }
        public bool cliacess { get; set; }
        public bool cliinserir { get; set; }
        public bool clialterar{ get; set; }
        public bool cliexcluir { get; set; }
        public bool funacess { get; set; }
        public bool funinserir { get; set; }
        public bool funalterar { get; set; }
        public bool funexcluir { get; set; }
        public bool fornacess { get; set; }
        public bool forninserir { get; set; }
        public bool fornalterar { get; set; }
        public bool fornexcluir { get; set; }
        public bool veicacess { get; set; }
        public bool veicinserir { get; set; }
        public bool veicalterar { get; set; }
        public bool veicexcluir { get; set; }
        public bool proacess { get; set; }
        public bool proinserir { get; set; }
        public bool proalterar { get; set; }
        public bool proexcluir { get; set; }
        public bool servacess { get; set; }
        public bool servinserir { get; set; }
        public bool servalterar { get; set; }
        public bool servexcluir { get; set; }
        public bool movivendasacess { get; set; }
        public bool movivendasinserir { get; set; }
        public bool movivendasalterar { get; set; }
        public bool movivendasexcluir { get; set; }
        public bool movicomprasacess { get; set; }
        public bool movicomprasinserir { get; set; }
        public bool movicomprasalterar { get; set; }
        public bool movicomprasexcluir { get; set; }
        public bool moviorcamentoacess { get; set; }
        public bool moviorcamentoinserir { get; set; }
        public bool moviorcamentoalterar { get; set; }
        public bool moviorcamentoexcluir { get; set; }
        public bool moviestoqueacess { get; set; }
        public bool moviestoqueinserir { get; set; }
        public bool moviestoquealterar { get; set; }
        public bool moviestoqueexcluir { get; set; }
        public bool relavendasacess { get; set; }
        public bool relavendasexcluir { get; set; }
        public bool relacomprasacess { get; set; }
        public bool relacomprasexcluir { get; set; }
        public bool relaestoqueacess { get; set; }
        public bool relaestoqueexcluir { get; set; }
        public bool relaorcamentoacess { get; set; }
        public bool relaorcamentoexcluir { get; set; }
        public bool usuarioacess { get; set; }
        public bool usuarioinserir { get; set; }
        public bool usuarioalterar { get; set; }
        public bool usuarioexcluir { get; set; }


    }
}
