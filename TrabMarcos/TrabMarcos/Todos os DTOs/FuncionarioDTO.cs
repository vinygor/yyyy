﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class FuncionarioDTO
    {
        public int id { get; set; }
        public DepartamentoDTO departamento { get; set; }
        public string rg { get; set; }
        public string nome { get; set; }
        public string senha { get; set; }
        public string cpf { get; set; }
        public string sexo { get; set; }
        public DateTime nascimento { get; set; }
        public DateTime observacao { get; set; }
        public string rua { get; set; }
        public string cargo { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string numero { get; set; }
        public string cep { get; set; }
        public string foto { get; set; }
        public string idDpto { get; set; }
        public string referencia { get; set; }
        public string complemento { get; set; }
        public string estado { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public string anotacoesfuncionario { get; set; }
        public string celular2 { get; set; }
        public string telefone2 { get; set; }
        public string imagem { get; set; }

    }
}
