﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class orcamentoDTO
    {
        public int id { get; set; }
        public FuncionarioDTO funcionario { get; set; }
        public string placa { get; set; }
        public string codigo { get; set; }
        public string modelo { get; set; }
        public string ano { get; set; }
        public string proprietario { get; set; }
        public bool aprovado { get; set; }

    }
}
