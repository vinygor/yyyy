﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class FornecedorDTO
    {
        public int id { get; set; }
        public UFDTO uf { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public bool sexo { get; set; }
        public DateTime GetDate { get; set; }
        public string rua { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string numero { get; set; }
        public string cep { get; set; }
        public string referencia { get; set; }
        public string complemento { get; set; }
        public string estado { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string telefonecomercial { get; set; }
        public string email { get; set; }
        public string ramal { get; set; }
        public string anotacoesfornecedor { get; set; }
        public string celular2 { get; set; }
        public string telefone2 { get; set; }
    }
}
