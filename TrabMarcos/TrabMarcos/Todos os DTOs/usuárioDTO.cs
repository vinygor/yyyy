﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class usuárioDTO
    {
        public int id { get; set; }
        public DepartamentoDTO Departamento { get; set; }
        public string nome { get; set; }
        public string usuario { get; set; }
        public perfilDTO Perfil { get; set; }
        public string senha { get; set; }
        public bool autorizaracess { get; set; }
        public string observacao { get; set; }
       


    }
}
