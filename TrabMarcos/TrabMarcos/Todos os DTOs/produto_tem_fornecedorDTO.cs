﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class produto_tem_fornecedorDTO
    {
        public FornecedorDTO fornecedor { get; set; }
        public produtoDTO produto { get; set; }

    }
}
