﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class veiculoDTO
    {
        public int id { get; set; }
        public modeloDTO codigomodelo { get; set; }
        public string placa { get; set; }
        public string proprietario { get; set; }
        public string modelo { get; set; }
        public string combustivel { get; set; }
        public string odometro { get; set; }
        public string cor { get; set; }
        public string anofabricacao { get; set; }
        public string observacoes { get; set; }



    }
}
