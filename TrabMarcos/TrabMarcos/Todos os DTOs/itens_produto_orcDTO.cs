﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.Todos_os_DTOs
{
    public class itens_produto_orcDTO
    {
        public produtoDTO produto { get; set; }
        public orcamentoDTO orcamento { get; set; }
    }
}
