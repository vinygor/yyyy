﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todos_os_Databases
{
    public class clientedatabase
    {
        public int Salvar (clienteDTO dto)
        {
            string script = @"INSERT INTO cliente ( nome, cpf , ds_pessoa ,sexo ,profissao ,ds_telefone,data_datanascimento, data_hoje,cidade,  bairro, rua  ,endereco ,numero  ,complemento  ,estado ,referencias,telefone ,celular ,tel_comercial1,ramal ,email ,anotaçoescliente,telefone1,telefone2,Cep) 
                                  VALUES (@nome, @cpf , @ds_pessoa ,@sexo ,@profissao ,@ds_telefone,@data_datanascimento, @data_hoje,@cidade ,@bairro,@rua  ,@endereco ,@numero  ,@complemento  ,@estado ,@referencias,@telefone ,@celular ,@tel_comercial1  ,@ramal ,@email ,@anotaçoescliente,@telefone1,@telefone2,@Cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_pessoa", dto.pessoa));
            parms.Add(new MySqlParameter("sexo", dto.Sexo));
            parms.Add(new MySqlParameter("profissao", dto.profissao));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("data_datanascimento", dto.datanascimento));
            parms.Add(new MySqlParameter("data_hoje", dto.datahojeclientedesde));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("rua", dto.rua));
            parms.Add(new MySqlParameter("endereco", dto.endereco));
            parms.Add(new MySqlParameter("numero", dto.numero));
            parms.Add(new MySqlParameter("complemento", dto.complemento));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("referencias", dto.referencia));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("celular", dto.celular1));
            parms.Add(new MySqlParameter("tel_comercial1", dto.telefonecormecial));
            parms.Add(new MySqlParameter("ramal", dto.ramal));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("anotaçoescliente", dto.anotaçoescliente));
            parms.Add(new MySqlParameter(" telefone1", dto.telefone));
            parms.Add(new MySqlParameter("telefone2", dto.telefone2));
            parms.Add(new MySqlParameter("Cep", dto.CEP));














            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public void Alterar(clienteDTO dto)
        {
            string script = @"UPDATE cliente 
 
SET

nome= @nome
cpf =@cpf
ds_pessoa=@ds_pessoa 
Sexo =@Sexo
profissao= @profissao
ds_telefone=@ds_telefone
 
datanascimento=@datanascimento
data_hoje =@data_hoje 
cidade=@cidade
bairro=@ bairro
rua  =@ rua  
endereco= @endereco 
numero  =@numero 
complemento= @complemento  
estado =@estado 
referencia=@referencia
telefone= @telefone
celular =@celular 
telefonecormecial=  @telefonecormecial 
ramal =@ramal
email =@email
anotaçoescliente=@anotaçoescliente
telefone1=@telefone1
telefone2=@telefone2
Cep =@CEP

  


                              WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_Cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_pessoa", dto.pessoa));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("endereço", dto.endereco));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("sexo", dto.Sexo));
            parms.Add(new MySqlParameter("profissao", dto.profissao));
            parms.Add(new MySqlParameter("datadenascimento", dto.datanascimento));
            parms.Add(new MySqlParameter("datahojeclientedesde", dto.datahojeclientedesde));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("rua", dto.rua));
            parms.Add(new MySqlParameter("numero", dto.numero));
            parms.Add(new MySqlParameter("complemento", dto.complemento));

            parms.Add(new MySqlParameter("referencia", dto.referencia));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("celular", dto.celular1));
            parms.Add(new MySqlParameter("telefonecormecial", dto.telefonecormecial));
            parms.Add(new MySqlParameter("ramal", dto.ramal));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("anotaçoescliente", dto.anotaçoescliente));
            parms.Add(new MySqlParameter(" telefone1", dto.telefone));
            parms.Add(new MySqlParameter("telefone2", dto.telefone2));
            parms.Add(new MySqlParameter("Cep", dto.CEP));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM cliente WHERE cliente = @cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<clienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM cliente WHERE nm_nome like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<clienteDTO> lista = new List<clienteDTO>();
            while (reader.Read())
            {
                clienteDTO dto = new clienteDTO();
                dto.Id = reader.GetInt32("cliente");
                dto.Nome = reader.GetString("nome");
                dto.CPF = reader.GetString("Cpf");
                dto.CPF = reader.GetString("pessoa");

                dto.bairro = reader.GetString("bairro");
                dto.endereco = reader.GetString("endereço");
                dto.estado = reader.GetString("estado");
                dto.telefone = reader.GetString("ds_telefone");


                dto.complemento = reader.GetString("complemento");
                dto.pessoa = reader.GetString(" pessoa");
                dto.profissao = reader.GetString("profissao");
                dto.datanascimento = reader.GetDateTime("datanascimento");
                dto.datahojeclientedesde = reader.GetDateTime("data_hoje");
                dto.referencia = reader.GetString("cidade ");

                dto.referencia = reader.GetString("referencia ");



                dto.celular1 = reader.GetString("celular");
                dto.telefonecormecial = reader.GetString("telefonecormecial");
                dto.ramal = reader.GetString("ramal");
                dto.email = reader.GetString("email");
                dto.rua = reader.GetString("rua ");
                dto.anotaçoescliente = reader.GetString("anotaçoescliente");
                dto.Sexo = reader.GetString("Sexo");
                dto.numero = reader.GetString("numero");
                dto.Sexo = reader.GetString("telefone1");
                dto.numero = reader.GetString("telefone2");
                dto.CEP = reader.GetString("Cep");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<clienteDTO> Listar()
        {
            string script = @"SELECT * FROM cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<clienteDTO> lista = new List<clienteDTO>();
            while (reader.Read())
            {
                clienteDTO dto = new clienteDTO();
                dto.Id = reader.GetInt32("cliente");
                dto.Nome = reader.GetString("nome");
                dto.CPF = reader.GetString("Cpf");
                dto.CPF = reader.GetString("pessoa");

                dto.bairro = reader.GetString("bairro");
                dto.endereco = reader.GetString("endereço");
                dto.estado = reader.GetString("estado");
                dto.telefone = reader.GetString("ds_telefone");


                dto.complemento = reader.GetString("complemento");
                dto.pessoa = reader.GetString(" pessoa");
                dto.profissao = reader.GetString("profissao");
                dto.datanascimento = reader.GetDateTime("datanascimento");
                dto.datahojeclientedesde = reader.GetDateTime("data_hoje");
                dto.referencia = reader.GetString("cidade ");

                dto.referencia = reader.GetString("referencia ");



                dto.celular1 = reader.GetString("celular");
                dto.telefonecormecial = reader.GetString("telefonecormecial");
                dto.ramal = reader.GetString("ramal");
                dto.email = reader.GetString("email");
                dto.rua = reader.GetString("rua ");
                dto.anotaçoescliente = reader.GetString("anotaçoescliente");
                dto.Sexo = reader.GetString("Sexo");
                dto.numero = reader.GetString("numero");
                dto.Sexo = reader.GetString("telefone1");
                dto.numero = reader.GetString("telefone2");
                dto.CEP = reader.GetString("Cep");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }



    }
}

