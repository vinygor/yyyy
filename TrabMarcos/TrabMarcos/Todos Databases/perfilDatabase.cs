﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;


namespace TrabMarcos.Todos_os_Databases
{
    public class perfilDatabase
    {
        public int Salvar(perfilDTO dto)
        {
            string script = @"INSERT INTO perfil ( 
                                        perfil, 
                                        nivel, 
                                        cliacess,
                                        cliinserir,
                                        clialterar,
                                        cliexcluir,
                                        funacess,
                                        funinserir,
                                        funalterar,
                                        funexcluir,
                                        fornacess,
                                        forninserir,
                                        fornalterar,
                                        fornexcluir,
                                        veicacess,
                                        veicinserir,
                                        veicalterar,
                                        veicexcluir,
                                        proacess,
                                        proinserir,
                                        proalterar,
                                        proexcluir,
                                        servacess,
                                        servinserir,
                                        servalterar,
                                        servexcluir,
                                        movivendasacess,
                                        movivendasinserir,
                                        movivendasalterar,
                                        movivendasexcluir,
                                        movicomprasacess,
                                        movicomprasinserir,
                                        movicomprasalterar,
                                        movicomprasexcluir,
                                        moviorcamentoacess,
                                        moviorcamentoinserir,
                                        moviorcamentoalterar,
                                        moviorcamentoexcluir,
                                        moviestoqueacess,
                                        moviestoqueinserir,
                                        moviestoquealterar,
                                        moviestoqueexcluir,
                                        relavendasacess,
                                        relavendasexcluir,
                                        relacomprasacess,
                                        relacomprasexcluir,
                                        relaorcamentoacess,
                                        relaorcamentoexcluir,
                                        relaestoqueacess,
                                        relaestoqueexcluir)
                             values (
                                        @perfil, 
                                        @nivel, 
                                        @cliacess,
                                        @cliinserir,
                                        @clialterar,
                                        @cliexcluir,
                                        @funacess,
                                        @funinserir,
                                        @funalterar,
                                        @funexcluir,
                                        @fornacess,
                                        @forninserir,
                                        @fornalterar,
                                        @fornexcluir,
                                        @veicacess,
                                        @veicinserir,
                                        @veicalterar,
                                        @veicexcluir,
                                        @proacess,
                                        @proinserir,
                                        @proalterar,
                                        @proexcluir,
                                        @servacess,
                                        @servinserir,
                                        @servalterar,
                                        @servexcluir,
                                        @movivendasacess,
                                        @movivendasinserir,
                                        @movivendasalterar,
                                        @movivendasexcluir,
                                        @movicomprasacess,
                                        @movicomprasinserir,
                                        @movicomprasalterar,
                                        @movicomprasexcluir,
                                        @moviorcamentoacess,
                                        @moviorcamentoinserir,
                                        @moviorcamentoalterar,
                                        @moviorcamentoexcluir,
                                        @moviestoqueacess,
                                        @moviestoqueinserir,
                                        @moviestoquealterar,
                                        @moviestoqueexcluir,
                                        @relavendasacess,
                                        @relavendasexcluir,
                                        @relacomprasacess,
                                        @relacomprasexcluir,
                                        @relaorcamentoacess,
                                        @relaorcamentoexcluir,
                                        @relaestoqueacess,
                                        @relaestoqueexcluir)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("perfil", dto.perfil));
            parms.Add(new MySqlParameter("nivel", dto.nivel));
            parms.Add(new MySqlParameter("cliacess", dto.cliacess));
            parms.Add(new MySqlParameter("cliinserir", dto.cliinserir));
            parms.Add(new MySqlParameter("clialterar", dto.clialterar));
            parms.Add(new MySqlParameter("cliexcluir", dto.cliexcluir));
            parms.Add(new MySqlParameter("funacess", dto.funacess));
            parms.Add(new MySqlParameter("funinserir", dto.funinserir));
            parms.Add(new MySqlParameter("funalterar", dto.funalterar));
            parms.Add(new MySqlParameter("funexcluir", dto.funexcluir));
            parms.Add(new MySqlParameter("fornacess", dto.fornacess));
            parms.Add(new MySqlParameter("forninserir", dto.forninserir));
            parms.Add(new MySqlParameter("fornalterar", dto.fornalterar));
            parms.Add(new MySqlParameter("fornexcluir", dto.fornexcluir));
            parms.Add(new MySqlParameter("veicacess", dto.veicacess));
            parms.Add(new MySqlParameter("veicinserir", dto.veicinserir));
            parms.Add(new MySqlParameter("veicalterar", dto.veicalterar));
            parms.Add(new MySqlParameter("veicexcluir", dto.veicexcluir));
            parms.Add(new MySqlParameter("proacess", dto.proacess));
            parms.Add(new MySqlParameter("proinserir", dto.proinserir));
            parms.Add(new MySqlParameter("proalterar", dto.proalterar));
            parms.Add(new MySqlParameter("proexcluir", dto.proexcluir));
            parms.Add(new MySqlParameter("servacess", dto.servacess));
            parms.Add(new MySqlParameter("servinserir", dto.servinserir));
            parms.Add(new MySqlParameter("servalterar", dto.servalterar));
            parms.Add(new MySqlParameter("servexcluir", dto.servexcluir));
            parms.Add(new MySqlParameter("movivendasacess", dto.movivendasacess));
            parms.Add(new MySqlParameter("movivendasinserir", dto.movivendasinserir));
            parms.Add(new MySqlParameter("movivendasalterar", dto.movivendasalterar));
            parms.Add(new MySqlParameter("movivendasexcluir", dto.movivendasexcluir));
            parms.Add(new MySqlParameter("movicomprasacess", dto.movicomprasacess));
            parms.Add(new MySqlParameter("movicomprasinserir", dto.movicomprasinserir));
            parms.Add(new MySqlParameter("movicomprasalterar", dto.movicomprasalterar));
            parms.Add(new MySqlParameter("movicomprasexcluir", dto.movicomprasexcluir));
            parms.Add(new MySqlParameter("moviorcamentoacess", dto.moviorcamentoacess));
            parms.Add(new MySqlParameter("moviorcamentoinserir", dto.moviorcamentoinserir));
            parms.Add(new MySqlParameter("moviorcamentoalterar", dto.moviorcamentoalterar));
            parms.Add(new MySqlParameter("moviorcamentoexcluir", dto.moviorcamentoexcluir));
            parms.Add(new MySqlParameter("moviestoqueacess", dto.moviestoqueacess));
            parms.Add(new MySqlParameter("moviestoqueinserir", dto.moviestoqueinserir));
            parms.Add(new MySqlParameter("moviestoquealterar", dto.moviestoquealterar));
            parms.Add(new MySqlParameter("moviestoqueexcluir", dto.moviestoqueexcluir));
            parms.Add(new MySqlParameter("relavendasacess", dto.relavendasacess));
            parms.Add(new MySqlParameter("relavendasexcluir", dto.relavendasexcluir));
            parms.Add(new MySqlParameter("relacomprasacess", dto.relacomprasacess));
            parms.Add(new MySqlParameter("relacomprasexcluir", dto.relacomprasexcluir));
            parms.Add(new MySqlParameter("relaestoqueacess", dto.relaestoqueacess));
            parms.Add(new MySqlParameter("relaestoqueexcluir", dto.relaestoqueexcluir));
            parms.Add(new MySqlParameter("relaorcamentoacess", dto.relaorcamentoacess));
            parms.Add(new MySqlParameter("relaorcamentoexcluir", dto.relaorcamentoexcluir));
            parms.Add(new MySqlParameter("usuarioacess", dto.usuarioacess));
            parms.Add(new MySqlParameter("usuarioinserir", dto.usuarioinserir));
            parms.Add(new MySqlParameter("usuarioalterar", dto.usuarioalterar));
            parms.Add(new MySqlParameter("usuarioexcluir", dto.usuarioalterar));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(perfilDTO dto)
        {
            string script = @"UPDATE perfil
 
                             SET

                                        perfil = @perfil, 
                                        nivel = @nivel,
                                        cliacess =  @cliacess,
                                        cliinserir = @cliinserir,
                                        clialterar = @clialterar,
                                        cliexcluir = @cliexcluir
                                        funacess = @funacess
                                        funinserir = @funinserir
                                        funalterar = @funalterar
                                        funexcluir = @funexcluir
                                        fornacess = @fornacess
                                        forninserir =  @forninserir
                                        fornalterar = @fornalterar
                                        fornexcluir = @fornexcluir
                                        veicinserir = @veicinserir
                                        veicalterar = @veicalterar
                                        veicexcluir = @veicexcluir
                                        proacess = @proacess
                                        proinserir = @proinserir
                                        proalterar = @proalterar
                                        proexcluir = @proexcluir
                                        servacess = @servacess
                                        servinserir = @servinserir
                                        servalterar = @servalterar
                                        servexcluir = @servexcluir
                                        movivendasacess = @movivendasacess
                                        movivendasinserir = @movivendasinserir
                                        movivendasalterar = @movivendasalterar
                                        movivendasexcluir = @movivendasexcluir
                                        movicomprasacess = @movicomprasacess
                                        movicomprasinserir = @movicomprasinserir
                                        movicomprasalterar =  @movicomprasalterar
                                        movicomprasexcluir =  @movicomprasexcluir
                                        moviorcamentoacess = @moviorcamentoacess
                                        moviorcamentoinserir = @moviorcamentoinserir
                                        moviorcamentoalterar = @moviorcamentoalterar
                                        moviorcamentoexcluir = @moviorcamentoexcluir
                                        moviestoqueacess = @moviestoqueacess
                                        moviestoqueinserir = @moviestoqueinserir
                                        moviestoquealterar = @moviestoquealterar
                                        moviestoqueexcluir = @moviestoqueexcluir
                                        relavendasacess = @relavendasacess
                                        relavendasexcluir = @relavendasexcluir
                                        relacomprasacess = @relacomprasacess
                                        relacomprasexcluir = @relacomprasexcluir
                                        relaorcamentoacess = @relaorcamentoacess
                                        relaorcamentoexcluir = @relaorcamentoexcluir
                                        relaestoqueacess = @relaestoqueacess
                                        relaestoqueexcluir = @relaestoqueexcluir


                              WHERE idperfil = @idperfil";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("perfil", dto.perfil));
            parms.Add(new MySqlParameter("nivel", dto.nivel));
            parms.Add(new MySqlParameter("cliacess", dto.cliacess));
            parms.Add(new MySqlParameter("cliinserir", dto.cliinserir));
            parms.Add(new MySqlParameter("clialterar", dto.clialterar));
            parms.Add(new MySqlParameter("cliexcluir", dto.cliexcluir));
            parms.Add(new MySqlParameter("funacess", dto.funacess));
            parms.Add(new MySqlParameter("funinserir", dto.funinserir));
            parms.Add(new MySqlParameter("funalterar", dto.funalterar));
            parms.Add(new MySqlParameter("funexcluir", dto.funexcluir));
            parms.Add(new MySqlParameter("fornacess", dto.fornacess));
            parms.Add(new MySqlParameter("forninserir", dto.forninserir));
            parms.Add(new MySqlParameter("fornalterar", dto.fornalterar));
            parms.Add(new MySqlParameter("fornexcluir", dto.fornexcluir));
            parms.Add(new MySqlParameter("veicacess", dto.veicacess));
            parms.Add(new MySqlParameter("veicinserir", dto.veicinserir));
            parms.Add(new MySqlParameter("veicalterar", dto.veicalterar));
            parms.Add(new MySqlParameter("veicexcluir", dto.veicexcluir));
            parms.Add(new MySqlParameter("proacess", dto.proacess));
            parms.Add(new MySqlParameter("proinserir", dto.proinserir));
            parms.Add(new MySqlParameter("proalterar", dto.proalterar));
            parms.Add(new MySqlParameter("proexcluir", dto.proexcluir));
            parms.Add(new MySqlParameter("servacess", dto.servacess));
            parms.Add(new MySqlParameter("servinserir", dto.servinserir));
            parms.Add(new MySqlParameter("servalterar", dto.servalterar));
            parms.Add(new MySqlParameter("servexcluir", dto.servexcluir));
            parms.Add(new MySqlParameter("movivendasacess", dto.movivendasacess));
            parms.Add(new MySqlParameter("movivendasinserir", dto.movivendasinserir));
            parms.Add(new MySqlParameter("movivendasalterar", dto.movivendasalterar));
            parms.Add(new MySqlParameter("movivendasexcluir", dto.movivendasexcluir));
            parms.Add(new MySqlParameter("movicomprasacess", dto.movicomprasacess));
            parms.Add(new MySqlParameter("movicomprasinserir", dto.movicomprasinserir));
            parms.Add(new MySqlParameter("movicomprasalterar", dto.movicomprasalterar));
            parms.Add(new MySqlParameter("movicomprasexcluir", dto.movicomprasexcluir));
            parms.Add(new MySqlParameter("moviorcamentoacess", dto.moviorcamentoacess));
            parms.Add(new MySqlParameter("moviorcamentoinserir", dto.moviorcamentoinserir));
            parms.Add(new MySqlParameter("moviorcamentoalterar", dto.moviorcamentoalterar));
            parms.Add(new MySqlParameter("moviorcamentoexcluir", dto.moviorcamentoexcluir));
            parms.Add(new MySqlParameter("moviestoqueacess", dto.moviestoqueacess));
            parms.Add(new MySqlParameter("moviestoqueinserir", dto.moviestoqueinserir));
            parms.Add(new MySqlParameter("moviestoquealterar", dto.moviestoquealterar));
            parms.Add(new MySqlParameter("moviestoqueexcluir", dto.moviestoqueexcluir));
            parms.Add(new MySqlParameter("relavendasacess", dto.relavendasacess));
            parms.Add(new MySqlParameter("relavendasexcluir", dto.relavendasexcluir));
            parms.Add(new MySqlParameter("relacomprasacess", dto.relacomprasacess));
            parms.Add(new MySqlParameter("relacomprasexcluir", dto.relacomprasexcluir));
            parms.Add(new MySqlParameter("relaestoqueacess", dto.relaestoqueacess));
            parms.Add(new MySqlParameter("relaestoqueexcluir", dto.relaestoqueexcluir));
            parms.Add(new MySqlParameter("relaorcamentoacess", dto.relaorcamentoacess));
            parms.Add(new MySqlParameter("relaorcamentoexcluir", dto.relaorcamentoexcluir));
            parms.Add(new MySqlParameter("usuarioacess", dto.usuarioacess));
            parms.Add(new MySqlParameter("usuarioinserir", dto.usuarioinserir));
            parms.Add(new MySqlParameter("usuarioalterar", dto.usuarioalterar));
            parms.Add(new MySqlParameter("usuarioexcluir", dto.usuarioalterar));

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM perfil  WHERE perfil = @perfil";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("perfil", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<perfilDTO> Consultar(string perfil)
        {
            string script = @"SELECT * FROM perfil WHERE perfil like @perfil";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("perfil", perfil + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<perfilDTO> lista = new List<perfilDTO>();
            while (reader.Read())
            {
                perfilDTO dto = new perfilDTO();

                dto.perfil = reader.GetString("perfil");
                dto.nivel = reader.GetString("nivel");
                dto.cliacess = reader.GetBoolean("cliacess");
                dto.cliinserir = reader.GetBoolean("cliinserir");
                dto.clialterar = reader.GetBoolean("clialterar");
                dto.cliexcluir = reader.GetBoolean("cliexcluir");
                dto.funacess = reader.GetBoolean("funacess");
                dto.funinserir = reader.GetBoolean("funinserir");
                dto.funalterar = reader.GetBoolean("funalterar");
                dto.funexcluir = reader.GetBoolean("funexcluir");
                dto.fornacess = reader.GetBoolean("fornacess");
                dto.forninserir = reader.GetBoolean("forninserir");
                dto.fornalterar = reader.GetBoolean("fornalterar");
                dto.fornexcluir = reader.GetBoolean("fornexcluir");
                dto.veicacess = reader.GetBoolean("veicacess ");
                dto.veicinserir = reader.GetBoolean("veicinserir");
                dto.veicalterar = reader.GetBoolean("veicalterar");
                dto.veicexcluir = reader.GetBoolean("veicexcluir");
                dto.proacess = reader.GetBoolean("proacess");
                dto.proinserir = reader.GetBoolean("proinserir");
                dto.proalterar = reader.GetBoolean("proalterar");
                dto.proexcluir = reader.GetBoolean("proexcluir");
                dto.servacess = reader.GetBoolean("servacess");
                dto.servinserir = reader.GetBoolean("servinserir");
                dto.servalterar = reader.GetBoolean("servalterar");
                dto.servexcluir = reader.GetBoolean("servexcluir");
                dto.movivendasacess = reader.GetBoolean("movivendasacess");
                dto.movivendasinserir = reader.GetBoolean("movivendasinserir");
                dto.movivendasalterar = reader.GetBoolean("movivendasalterar");
                dto.movivendasexcluir = reader.GetBoolean("movivendasexcluir");
                dto.movicomprasacess = reader.GetBoolean("movicomprasacess");
                dto.movicomprasinserir = reader.GetBoolean("movicomprasinserir");
                dto.movicomprasalterar = reader.GetBoolean("movicomprasalterar");
                dto.movicomprasexcluir = reader.GetBoolean("movicomprasexcluir");
                dto.moviorcamentoacess = reader.GetBoolean("moviorcamentoacess");
                dto.moviorcamentoinserir = reader.GetBoolean("moviorcamentoinserir");
                dto.moviorcamentoalterar = reader.GetBoolean("moviorcamentoalterar");
                dto.moviorcamentoexcluir = reader.GetBoolean("moviorcamentoexcluir");
                dto.moviestoqueacess = reader.GetBoolean("moviestoqueacess");
                dto.moviestoqueinserir = reader.GetBoolean("moviestoqueinserir");
                dto.moviestoquealterar = reader.GetBoolean("moviestoquealterar");
                dto.moviestoqueexcluir = reader.GetBoolean("moviestoqueexcluir");
                dto.relavendasacess = reader.GetBoolean("relavendasacess");
                dto.relavendasexcluir = reader.GetBoolean("relavendasexcluir");
                dto.relacomprasacess = reader.GetBoolean("relacomprasacess");
                dto.relacomprasexcluir = reader.GetBoolean("relacomprasexcluir ");
                dto.relaestoqueacess = reader.GetBoolean("relaestoqueacess");
                dto.relaestoqueexcluir = reader.GetBoolean("relaestoqueexcluir");
                dto.relaorcamentoacess = reader.GetBoolean("relaorcamentoacess");
                dto.relaorcamentoexcluir = reader.GetBoolean("relaorcamentoexcluir");
                dto.usuarioacess = reader.GetBoolean("usuarioacess");
                dto.usuarioinserir = reader.GetBoolean("usuarioacess");
                dto.usuarioalterar = reader.GetBoolean("usuarioacess");
                dto.usuarioexcluir = reader.GetBoolean("usuarioacess");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<perfilDTO> Listar()
        {
            string script = @"SELECT * FROM perfil";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<perfilDTO> lista = new List<perfilDTO>();
            while (reader.Read())
            {
                perfilDTO dto = new perfilDTO();
                dto.perfil = reader.GetString("perfil");
                dto.nivel = reader.GetString("nivel");
                dto.cliacess = reader.GetBoolean("cliacess");
                dto.cliinserir = reader.GetBoolean("cliinserir");
                dto.clialterar = reader.GetBoolean("clialterar");
                dto.cliexcluir = reader.GetBoolean("cliexcluir");
                dto.funacess = reader.GetBoolean("funacess");
                dto.funinserir = reader.GetBoolean("funinserir");
                dto.funalterar = reader.GetBoolean("funalterar");
                dto.funexcluir = reader.GetBoolean("funexcluir");
                dto.fornacess = reader.GetBoolean("fornacess");
                dto.forninserir = reader.GetBoolean("forninserir");
                dto.fornalterar = reader.GetBoolean("fornalterar");
                dto.fornexcluir = reader.GetBoolean("fornexcluir");
                dto.veicacess = reader.GetBoolean("veicacess ");
                dto.veicinserir = reader.GetBoolean("veicinserir");
                dto.veicalterar = reader.GetBoolean("veicalterar");
                dto.veicexcluir = reader.GetBoolean("veicexcluir");
                dto.proacess = reader.GetBoolean("proacess");
                dto.proinserir = reader.GetBoolean("proinserir");
                dto.proalterar = reader.GetBoolean("proalterar");
                dto.proexcluir = reader.GetBoolean("proexcluir");
                dto.servacess = reader.GetBoolean("servacess");
                dto.servinserir = reader.GetBoolean("servinserir");
                dto.servalterar = reader.GetBoolean("servalterar");
                dto.servexcluir = reader.GetBoolean("servexcluir");
                dto.movivendasacess = reader.GetBoolean("movivendasacess");
                dto.movivendasinserir = reader.GetBoolean("movivendasinserir");
                dto.movivendasalterar = reader.GetBoolean("movivendasalterar");
                dto.movivendasexcluir = reader.GetBoolean("movivendasexcluir");
                dto.movicomprasacess = reader.GetBoolean("movicomprasacess");
                dto.movicomprasinserir = reader.GetBoolean("movicomprasinserir");
                dto.movicomprasalterar = reader.GetBoolean("movicomprasalterar");
                dto.movicomprasexcluir = reader.GetBoolean("movicomprasexcluir");
                dto.moviorcamentoacess = reader.GetBoolean("moviorcamentoacess");
                dto.moviorcamentoinserir = reader.GetBoolean("moviorcamentoinserir");
                dto.moviorcamentoalterar = reader.GetBoolean("moviorcamentoalterar");
                dto.moviorcamentoexcluir = reader.GetBoolean("moviorcamentoexcluir");
                dto.moviestoqueacess = reader.GetBoolean("moviestoqueacess");
                dto.moviestoqueinserir = reader.GetBoolean("moviestoqueinserir");
                dto.moviestoquealterar = reader.GetBoolean("moviestoquealterar");
                dto.moviestoqueexcluir = reader.GetBoolean("moviestoqueexcluir");
                dto.relavendasacess = reader.GetBoolean("relavendasacess");
                dto.relavendasexcluir = reader.GetBoolean("relavendasexcluir");
                dto.relacomprasacess = reader.GetBoolean("relacomprasacess");
                dto.relacomprasexcluir = reader.GetBoolean("relacomprasexcluir ");
                dto.relaestoqueacess = reader.GetBoolean("relaestoqueacess");
                dto.relaestoqueexcluir = reader.GetBoolean("relaestoqueexcluir");
                dto.relaorcamentoacess = reader.GetBoolean("relaorcamentoacess");
                dto.relaorcamentoexcluir = reader.GetBoolean("relaorcamentoexcluir");
                dto.usuarioacess = reader.GetBoolean("usuarioacess");
                dto.usuarioinserir = reader.GetBoolean("usuarioacess");
                dto.usuarioalterar = reader.GetBoolean("usuarioacess");
                dto.usuarioexcluir = reader.GetBoolean("usuarioacess");
            }
            reader.Close();

            return lista;
        }

        public perfilDTO Permitir (usuárioDTO id)
        {
            string script =
                @"select * from usuario inner join perfil on usuario.idusuario where usuario = @usuario
                     and senha   = @senha";
    


            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("usuario", id));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            perfilDTO dt = null;

            if (reader.Read() == true)
            {

 
                perfilDTO Perfil = new perfilDTO();
                dt.perfil = reader.GetString("perfil");
                dt.nivel = reader.GetString("nivel");
                dt.cliacess = reader.GetBoolean("cliacess");
                dt.cliinserir = reader.GetBoolean("cliinserir");
                dt.clialterar = reader.GetBoolean("clialterar");
                dt.cliexcluir = reader.GetBoolean("cliexcluir");
                dt.funacess = reader.GetBoolean("funacess");
                dt.funinserir = reader.GetBoolean("funinserir");
                dt.funalterar = reader.GetBoolean("funalterar");
                dt.funexcluir = reader.GetBoolean("funexcluir");
                dt.fornacess = reader.GetBoolean("fornacess");
                dt.forninserir = reader.GetBoolean("forninserir");
                dt.fornalterar = reader.GetBoolean("fornalterar");
                dt.fornexcluir = reader.GetBoolean("fornexcluir");
                dt.veicacess = reader.GetBoolean("veicacess ");
                dt.veicinserir = reader.GetBoolean("veicinserir");
                dt.veicalterar = reader.GetBoolean("veicalterar");
                dt.veicexcluir = reader.GetBoolean("veicexcluir");
                dt.proacess = reader.GetBoolean("proacess");
                dt.proinserir = reader.GetBoolean("proinserir");
                dt.proalterar = reader.GetBoolean("proalterar");
                dt.proexcluir = reader.GetBoolean("proexcluir");
                dt.servacess = reader.GetBoolean("servacess");
                dt.servinserir = reader.GetBoolean("servinserir");
                dt.servalterar = reader.GetBoolean("servalterar");
                dt.servexcluir = reader.GetBoolean("servexcluir");
                dt.movivendasacess = reader.GetBoolean("movivendasacess");
                dt.movivendasinserir = reader.GetBoolean("movivendasinserir");
                dt.movivendasalterar = reader.GetBoolean("movivendasalterar");
                dt.movivendasexcluir = reader.GetBoolean("movivendasexcluir");
                dt.movicomprasacess = reader.GetBoolean("movicomprasacess");
                dt.movicomprasinserir = reader.GetBoolean("movicomprasinserir");
                dt.movicomprasalterar = reader.GetBoolean("movicomprasalterar");
                dt.movicomprasexcluir = reader.GetBoolean("movicomprasexcluir");
                dt.moviorcamentoacess = reader.GetBoolean("moviorcamentoacess");
                dt.moviorcamentoinserir = reader.GetBoolean("moviorcamentoinserir");
                dt.moviorcamentoalterar = reader.GetBoolean("moviorcamentoalterar");
                dt.moviorcamentoexcluir = reader.GetBoolean("moviorcamentoexcluir");
                dt.moviestoqueacess = reader.GetBoolean("moviestoqueacess");
                dt.moviestoqueinserir = reader.GetBoolean("moviestoqueinserir");
                dt.moviestoquealterar = reader.GetBoolean("moviestoquealterar");
                dt.moviestoqueexcluir = reader.GetBoolean("moviestoqueexcluir");
                dt.relavendasacess = reader.GetBoolean("relavendasacess");
                dt.relavendasexcluir = reader.GetBoolean("relavendasexcluir");
                dt.relacomprasacess = reader.GetBoolean("relacomprasacess");
                dt.relacomprasexcluir = reader.GetBoolean("relacomprasexcluir ");
                dt.relaestoqueacess = reader.GetBoolean("relaestoqueacess");
                dt.relaestoqueexcluir = reader.GetBoolean("relaestoqueexcluir");
                dt.relaorcamentoacess = reader.GetBoolean("relaorcamentoacess");
                dt.relaorcamentoexcluir = reader.GetBoolean("relaorcamentoexcluir");
                dt.usuarioacess = reader.GetBoolean("usuarioacess");
                dt.usuarioinserir = reader.GetBoolean("usuarioacess");
                dt.usuarioalterar = reader.GetBoolean("usuarioacess");
                dt.usuarioexcluir = reader.GetBoolean("usuarioacess");
            }
            return dt;
        }

    }
}











