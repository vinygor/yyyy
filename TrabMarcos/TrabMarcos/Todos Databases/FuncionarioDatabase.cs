﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todos_os_Databases
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO funcionario ( sexo, senha , nome ,data_nascimento ,cpf ,rg,complemento, rua,cargo,  observacao, id_dpto  ,foto ,bairro  ,cidade  ,referencia,numero ,estado,email,telefone ,celular,cpe) 
                                  VALUES (@sexo, @senha , @nome ,@data_nascimento ,@cpf ,@rg,@complemento, @rua,@cargo,  @observacao, @id_dpto  ,@foto ,@bairro  ,@cidade  ,@referencia,@numero ,@estado,@email,@telefone ,@celular,@cpe)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("data_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("cpf", dto.cpf));
            parms.Add(new MySqlParameter("rg", dto.rg));
            parms.Add(new MySqlParameter("complemento", dto.complemento));
            parms.Add(new MySqlParameter("rua", dto.rua));
            parms.Add(new MySqlParameter("cargo", dto.cargo));
            parms.Add(new MySqlParameter("observacao", dto.observacao));
            parms.Add(new MySqlParameter("id_dpto", dto.idDpto));
            parms.Add(new MySqlParameter("foto", dto.foto));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("referencia", dto.referencia));
            parms.Add(new MySqlParameter("numero", dto.numero));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("celular", dto.celular));
            parms.Add(new MySqlParameter("cpe", dto.cep));
            














            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE funcionario 
 
SET
sexo=@sexo
senha=@senha
nome=@nome                    
data_nascimento=@data_nascimento
cpf=@cpf
rg=@rg
complemento=@complemento
rua=@rua
cargo=@cargo
observacao=@observacao
id_dpto=@id_dpto  
foto=@foto 
bairro=@bairro  
cidade =@cidade  
referencia=@referencia
numero =@numero 
estado=@estado
email=@email
telefone=@telefone 
celular=@celular
cpe=@cpe

WHERE idfuncionario = @idfuncionario ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("data_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("cpf", dto.cpf));
            parms.Add(new MySqlParameter("rg", dto.rg));
            parms.Add(new MySqlParameter("complemento", dto.complemento));
            parms.Add(new MySqlParameter("rua", dto.rua));
            parms.Add(new MySqlParameter("cargo", dto.cargo));
            parms.Add(new MySqlParameter("observacao", dto.observacao));
            parms.Add(new MySqlParameter("id_dpto", dto.idDpto));
            parms.Add(new MySqlParameter("foto", dto.foto));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("referencia", dto.referencia));
            parms.Add(new MySqlParameter("numero", dto.numero));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("email", dto.email));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("celular", dto.celular));
            parms.Add(new MySqlParameter("cpe", dto.cep));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM funcionario WHERE idfuncionario= @idfuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idfuncionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FuncionarioDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM funcionario WHERE nome like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                

                dto.id = reader.GetInt32("idfuncionario");

                dto.sexo = reader.GetString("sexo");
                dto.senha = reader.GetString("senha");
                dto.nome = reader.GetString("nome");
                dto.nascimento = reader.GetDateTime("data_nascimento");
                dto.cpf = reader.GetString("cpf");
                dto.rg = reader.GetString("rg");
                dto.complemento = reader.GetString("complemento");
                dto.rua = reader.GetString("rua");
                dto.cargo = reader.GetString("cargo");
                dto.observacao = reader.GetDateTime("observacao");
                dto.idDpto = reader.GetString("id_dpto");
                dto.foto = reader.GetString("foto");
                dto.bairro = reader.GetString("bairro");
                dto.cidade = reader.GetString("cidade");
                dto.referencia = reader.GetString("referencia");
                dto.numero = reader.GetString("numero");
                dto.estado = reader.GetString("estado");
                dto.email = reader.GetString("email");
                dto.telefone = reader.GetString("telefone");
                dto.celular = reader.GetString("celular");
                dto.cep = reader.GetString("cpe");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();


                dto.id = reader.GetInt32("idfuncionario");

                dto.sexo = reader.GetString("sexo");
                dto.senha = reader.GetString("senha");
                dto.nome = reader.GetString("nome");
                dto.nascimento = reader.GetDateTime("data_nascimento");
                dto.cpf = reader.GetString("cpf");
                dto.rg = reader.GetString("rg");
                dto.complemento = reader.GetString("complemento");
                dto.rua = reader.GetString("rua");
                dto.cargo = reader.GetString("cargo");
                dto.observacao = reader.GetDateTime("observacao");
                dto.idDpto = reader.GetString("id_dpto");
                dto.foto = reader.GetString("foto");
                dto.bairro = reader.GetString("bairro");
                dto.cidade = reader.GetString("cidade");
                dto.referencia = reader.GetString("referencia");
                dto.numero = reader.GetString("numero");
                dto.estado = reader.GetString("estado");
                dto.email = reader.GetString("email");
                dto.telefone = reader.GetString("telefone");
                dto.celular = reader.GetString("celular");
                dto.cep = reader.GetString("cpe");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }



    }
}

