﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.connection;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todos_os_Databases
{
    public class usuárioDatabase
    {
        public int Salvar(usuárioDTO dto)
        {
            string script = @"INSERT INTO cliente ( nm_nome, ds_departamento , ds_usuario , ds_senha , ds_autorizaracess ,ds_observacao) 
                                  VALUES (@nm_nome, @ds_departamento , @ds_usuario , @ds_senha , @ds_autorizaracess ,@ds_observacao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("departamento", dto.Departamento));
            parms.Add(new MySqlParameter("usuario", dto.usuario));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("autorizaracess", dto.autorizaracess));
            parms.Add(new MySqlParameter("observacao", dto.observacao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        internal usuárioDatabase logar(string usuario, string senha)
        {
            throw new NotImplementedException();
        }

        public void Alterar(usuárioDTO dto)
        {
            string script = @"UPDATE usuario
 
SET

nome = @nome
departamento = @ds_departamento
usuario = @ds_usuario
senha = @senha
autorizaracess = @ds_autorizaracess
telefone = @ds_telefone
observacao = @ds_observacao


                              WHERE id_usuario = @id_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("departamento", dto.Departamento));
            parms.Add(new MySqlParameter("usuario", dto.usuario));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("autorizaracess", dto.autorizaracess));
            parms.Add(new MySqlParameter("observacao", dto.observacao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM usuario  WHERE usuario = @usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("usuario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<usuárioDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM usuario WHERE nm_nome like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<usuárioDTO> lista = new List<usuárioDTO>();
            while (reader.Read())
            {
                usuárioDTO dto = new usuárioDTO();
                dto.id = reader.GetInt32("id");
                dto.nome = reader.GetString("nome");
                dto.usuario = reader.GetString("usuario");
                dto.senha = reader.GetString("senha");
                dto.autorizaracess = reader.GetBoolean("autorizaracess");
                dto.observacao = reader.GetString("observacao");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<usuárioDTO> Listar()
        {
            string script = @"SELECT * FROM usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<usuárioDTO> lista = new List<usuárioDTO>();
            while (reader.Read())
            {
                usuárioDTO dto = new usuárioDTO();
                dto.id = reader.GetInt32("id");
                dto.nome = reader.GetString("nome");
                dto.Departamento.id = reader.GetInt32("id");
                dto.usuario = reader.GetString("usuario");
                dto.senha = reader.GetString("senha");
                dto.autorizaracess = reader.GetBoolean("autorizaracess");
                dto.observacao = reader.GetString("observacao");
            }
            reader.Close();

            return lista;
        }





        public usuárioDTO Logar(string usuario, string senha)
        {

            string script =
                @"select * from usuario where usuario = @usuario and senha = @senha";



            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("ds_usuario", usuario));
            parametros.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            usuárioDTO dt = null;

            if (reader.Read() == true)
            {

                usuárioDTO dto = new usuárioDTO();
                dt.id = reader.GetInt32("id_usuario");
                dt.nome = reader.GetString("nm_nome");
                dt.usuario = reader.GetString("ds_usuario");
                dt.senha = reader.GetString("ds-senha");
                dt.autorizaracess = reader.GetBoolean("ds_autorizaracess");
                dt.observacao = reader.GetString("ds_observacao");
            }

            return dt;
        }
    }
}








