﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.Tela;
using TrabMarcos.Todos_os_Databases;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todas_Business
{
   public class usuárioBusiness
    {

        public class LoginBusiness
        {
            public int Salvar(usuárioDTO dto )
            {
                if (dto.nome == string.Empty)
                {
                    throw new Exception("Nome do usuário é obrigatório.");
                }
                if (dto.usuario == string.Empty)
                {
                    throw new Exception("Deve-se inserir um login para o usuário.");
                }
                if (dto.senha == string.Empty)
                {
                    throw new Exception("Deve-se inserir uma senha para o usuário.");
                }
                usuárioDatabase usuarioDB = new usuárioDatabase();
                int id = usuarioDB.Salvar(dto);
                return id;
            }

            public List<usuárioDTO> Listar()
            {
                usuárioDatabase usuarioDB = new usuárioDatabase();
                List<usuárioDTO> usuarios = usuarioDB.Listar();
                return usuarios;
            }

            public void Alterar(usuárioDTO usuario)
            {
                usuárioDatabase usuarioDB = new usuárioDatabase();
                usuarioDB.Alterar(usuario);
            }

            public void Remover(int idusuario)
            {
                usuárioDatabase usuarioDB = new usuárioDatabase();
                usuarioDB.Remover(idusuario);
            }

            public usuárioDatabase logar(string usuario, string senha)
                {
                    usuárioDatabase db = new usuárioDatabase();
                    usuárioDatabase dt = db.logar(usuario, senha);

                    return dt;

                }
           
        }
    }
}
    

