﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.Todos_os_Databases;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todas_Business
{
    public class perfilBusiness
    {
        public int Salvar(perfilDTO dto)
        {
            if (dto.perfil == string.Empty)
            {
                throw new Exception("Perfil do usuário é obrigatório.");
            }
            if (dto. nivel == string.Empty)
            {
                throw new Exception("Deve-se inserir um nível de usuário.");
            }
           
            perfilDatabase perfilDB = new perfilDatabase();
            int id = perfilDB.Salvar(dto);
            return id;
        }

        public List<perfilDTO> Listar()
        {
            perfilDatabase perfilDB = new perfilDatabase();
            List<perfilDTO> perfis = perfilDB.Listar();
            return perfis;
        }

        public void Alterar(perfilDTO perfil)
        {
            perfilDatabase perfilDB = new perfilDatabase();
            perfilDB.Alterar(perfil);
        }

        public void Remover(int idperfil)
        {
            perfilDatabase perfilDB = new perfilDatabase();
            perfilDB.Remover(idperfil);
        }

        

        

    }
}
