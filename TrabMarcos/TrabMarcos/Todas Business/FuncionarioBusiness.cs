﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrabMarcos.Todos_os_Databases;
using TrabMarcos.Todos_os_DTOs;

namespace TrabMarcos.Todas_Business
{
    class FuncionarioBusiness
    {

        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }



        public List<FuncionarioDTO> Consultar(string cliente)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db = new FuncionarioDatabase();
            return db.Consultar(cliente);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }
        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }
    }
}
