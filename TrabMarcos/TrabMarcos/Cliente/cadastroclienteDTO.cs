﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrabMarcos.pasta
{
    class cadastroclienteDTO
    {
         
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string pessoa { get; set; }
        public string Sexo { get; set; }
        public string profissao { get; set; }
        public string telefone { get; set; }
         public DateTime datanascimento { get; set; }
        
        public string celular { get; set; }
        public string telefone1 { get; set; }
        public string telefone2 { get; set; }
        public DateTime datahojeclientedesde { get; set; }
         public string   cidade { get; set; }
        public string telefonecormecial { get; set; }

        public string anotaçoescliente { get; set; }
        public string email { get; set; }
        public string ramal { get; set; }

        public string CEP { get; set; }
        public string bairro { get; set; }
        public string rua { get; set; }
        public string endereco { get; set; }
        public string numero { get; set; }
         public string complemento { get; set; }
         public string estado { get; set; }
        public string referencia{ get; set; }
        
    }
}
